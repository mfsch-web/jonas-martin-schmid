#!/bin/bash
set -euo pipefail

SITE=public
PANDOC_VERSION=2.11.2

# get pandoc if not installed
if [[ ! -f tools/pandoc-${PANDOC_VERSION}/bin/pandoc ]]; then
    mkdir -p tools
    wget -O tools/pandoc.tar.gz https://github.com/jgm/pandoc/releases/download/${PANDOC_VERSION}/pandoc-${PANDOC_VERSION}-linux-amd64.tar.gz
    tar -xzf tools/pandoc.tar.gz -C tools
fi

# build sites and copy to output
mkdir -p $SITE
cp -r jms-new/* ${SITE}/
cp -r dcgraphics ${SITE}/
cd jms && make && cd .. && cp -r jms/public ${SITE}/jms
cd hormonfabrik && make && cd .. && cp -r hormonfabrik/public ${SITE}/hormonfabrik
cd dcr && make && cd .. && cp -r dcr/public ${SITE}/dirtycatrecords
