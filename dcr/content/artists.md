---
title: ARTISTS
---

<p><table border="0" cellpadding="5"><tbody>

<tr>
<td align="left" valign="top"><a href="artists/adis-gile">
  <img src="img/artists/Adis-Gile_small.jpg" alt="Adis Gile" width="200" />
  <br />ADIS GILE<br />(TECHHOUSE/HOUSE)
</a></td>
<td align="left" valign="top"><a href="artists/angelo-fonfara">
  <img src="img/artists/Angelo-Fonfara_small.jpg" alt="Angelo Fonfara" width="200" />
  <br />ANGELO FONFARA<br />(BREAKBEAT/DNB/ATERNATIVE)
</a></td>
<td align="left" valign="top"><a href="artists/antimaterium">
  <img src="img/artists/Antimaterium_small.jpg" alt="Antimaterium" width="200" height="200" />
  <br />ANTIMATERIUM<br />(TECHNO)
</a></td>
<td align="left" valign="top"><a href="artists/aurel-den-bossa">
  <img src="img/artists/Aurel-den-Bossa_small.jpg" alt="Aurel den Bossa" width="200" height="200" />
  <br />AUREL DEN BOSSA<br />(TECHNO/DEEP HOUSE)
</a></td>
</tr>

<tr>
<td align="left" valign="top"><a href="artists/buttheads">
  <img src="img/artists/Buttheads_small.jpg" alt="Buttheads" width="200" />
  <br />BUTTHEADS<br />(ELECTRO/HOUSE/DISCO)
</a></td>
<td align="left" valign="top"><a href="artists/beat-frei">
  <img src="img/artists/Beat-Frei_small.jpg" alt="Beat Frea" width="200" height="200" />
  <br />BEAT FREI<br />(TECHNO/HOUSE)
</a></td>
<td align="left" valign="top"><a href="artists/Cuscino">
  <img src="img/artists/Cuscino_small.jpg" alt="Cuscino" width="200" height="200" />
  <br />CUSCINO<br />(HOUSE)
</a></td>
<td align="left" valign="top"><a href="artists/dani-corbalan">
  <img src="img/artists/Dani-Corbalan_small.jpg" alt="Dani Corbalan" width="200" height="200" />
  <br />DANI CORBALAN<br />(HOUSE)
</a></td>
</tr>

<tr>
<td align="left" valign="top"><a href="artists/digitule">
  <img src="img/artists/Digitule_small.jpg" alt="Digitule" width="200" height="200" />
  <br />DIGITULE<br />(DEEP TECHNO/DEEP HOUSE)
</a></td>
<td align="left" valign="top"><a href="artists/dirty-cat">
  <img src="img/artists/Dirty-Cat_small.jpg" alt="Dirty Cat" width="200" />
  <br />DIRTY CAT<br />(TECHNO/DEEP HOUSE)
</a></td>
<td align="left" valign="top"><a href="artists/hansgod">
  <img src="img/artists/Hansgod_small.jpg" alt="Hansgod" width="200" height="200" />
  <br />HANSGOD<br />(TECHNO/HOUSE/MINMAL)
</a></td>
<td align="left" valign="top"><a href="artists/hobo-baggins">
  <img src="img/artists/Hobo-Baggins_small.jpg" alt="Hobo Baggins" width="200" height="200" />
  <br />HOBO BAGGINS<br />(TECHNO/DEEP HOUSE)
</a></td>
</tr>

<tr>
<td align="left" valign="top"><a href="artists/jan-ploetzlich">
  <img src="img/artists/Jan-Ploetzlich_small.jpg" alt="Jan Plötzlich" width="200" height="200" />
  <br />JAN PLÖTZLICH<br />(DOWNTEMPO/DEEP HOUSE)
</a></td>
<td align="left" valign="top"><a href="artists/josh-tree">
  <img src="img/artists/Josh-Tree_small.jpg" alt="Josh Tree" width="200" />
  <br />JOSH TREE<br />(TECHNO/MINIMAL)
</a></td>
<td align="left" valign="top"><a href="artists/k-n-i-g-h-t">
  <img src="img/artists/K-N-I-G-H-T_small.jpg" alt="K-N-I-G-H-T" width="200" height="200" />
  <br />K-N-I-G-H-T<br />(TECHNO)
</a></td>
<td align="left" valign="top"><a href="artists/kabelmann">
  <img src="img/artists/Kabelmann_small.jpg" alt="Kabelmann" width="200" />
  <br />KABELMANN<br />(ELECTRONICA/EXPERIMENTAL)
</a></td>
</tr>

<tr>
<td align="left" valign="top"><a href="artists/niki-plisic">
  <img src="img/artists/Niki-Plisic_small.jpg" alt="Niki Plisic" width="200" height="200" />
  <br />NIKI PLISIC<br />(TECHNO/HOUSE)
</a></td>
<td align="left" valign="top"><a href="artists/outpost-progress">
  <img src="img/artists/Outpost-of-Progress_small.jpg" alt="Outpost of Progress" width="200" height="200" />
  <br />OUTPOST OF PROGRESS<br />(ALTERNATIVE/ELECTRONICA)
</a></td>
<td align="left" valign="top"><a href="artists/projekt-gestalten">
  <img src="img/artists/Projekt-Gestalten_small.jpg" alt="Projekt Gestalten" width="200" height="200" />
  <br />PROJEKT GESTALTEN<br />(TECHNO/HOUSE)
</a></td>
<td align="left" valign="top"><a href="artists/tammo">
  <img src="img/artists/Tammo_small.jpg" alt="Tammo" width="200" />
  <br />TAMMO<br />(BREAKBEAT/ALTERNATIVE)
</a></td>
</tr>

<tr>
<td align="left" valign="top"><a href="artists/doc-atmosfearcrush-twilight-kamikaze">
  <img src="img/artists/Twilight-Kamikaze_small.jpg" alt="Twilight Kamikaze" width="200" height="200" />
  <br />DOC.ATMOSFEARCRUSH<br />TWILIGHT KAMIKAZE<br />(IDM)
</a></td>
<td align="left" valign="top"><a href="artists/trinovante">
  <img src="img/artists/TrinoVante_small.jpg" alt="TrinoVante" width="200" height="200" />
  <br />TRINOVANTE<br />(TECHNO/DEEPHOUSE)
</a></td>
<td align="left" valign="top"> </td>
<td align="left" valign="top"> </td>
</tr>

</tbody></table></p>
