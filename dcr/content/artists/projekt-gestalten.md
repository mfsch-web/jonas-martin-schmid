---
title: Projekt Gestalten
soundcloud-id: 263262
prelude: '<div id="ccm-layout-wrapper-2617" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><img border="0" class="ccm-image-block" alt="" src="../img/artists/Projekt-Gestalten.jpg" width="400" height="400" /><div id="blockStyle1509HauptbereichLayout1Cell1154" class=" ccm-block-styles" >'
---

</div></div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">PROJEKT GESTALTEN [Producer/DJ/Liveact]</span> </p>
<p>BERLIN/GERMANY</p>
<p><a href="http://soundcloud.com/gestalten" target="_blank">Soundcloud</a> | <a href="https://www.facebook.com/projektgestalten" target="_blank">Facebook</a> | <a href="http://www.residentadvisor.net/dj/projektgestalten" target="_blank">Resident Advisor</a> | <a href="http://www.projektgestalten.com/news.html">Official Website</a></p>
<p>iTunes | Beatport | Whatpeopleplay | Amazon<br /><br /></p><p>The Berlin-based Brazilian Diego Garcia could be described as an universal media artist. His alias is called Projekt Gestalten and combines various forms of digital media art. Garcia combines highly abstract video art and graphic work with an aggressive approach of techno music that can sometimes even detour into electronica, house and ambient areas. With residencies in the party labels Pornceptual, Verboten and Über, in Berlin, he performed his live act, where he manipulates over 4000 loops and samples on the fly combined with analog synths, in several clubs and festivals, sharing the booth with names like Ancient Methods, Bill Youngmann, Acid Pauli, NU, Anna Bolena and Subjected. As a producer, In the field of visual arts, Garcia had his pieces displayed in festivals and museums around the world and participated in a project conceptualized by acclaimed Danish movie director, Lars von Trier.</p><p> </p>
<h3>Releases</h3>
<p><strong>2015:</strong><a href="../releases/dcr032"> Life (DCRT032)</a></p></div><div class="ccm-spacer"></div></div></div></div>
