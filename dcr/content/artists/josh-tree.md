---
title: Josh Tree
soundcloud-id: 2002433
prelude: '<div id="ccm-layout-wrapper-2012" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-32-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-32-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:100%">&nbsp;</div><div class="ccm-spacer"></div></div></div></div><div id="ccm-layout-wrapper-2011" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-100-2" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-2 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-100-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><div id="blockStyle878HauptbereichLayout2Cell159" class=" ccm-block-styles" ><img border="0" class="ccm-image-block" alt="" src="../img/artists/Josh-Tree.jpg"  width="300" height="366" /></div><div id="blockStyle1137HauptbereichLayout2Cell160" class=" ccm-block-styles" >'
---

</div></div><div class="ccm-layout-100-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">JOSH TREE [Producer/DJ/Live-Act]</span></p>
<p>BASEL/SWITZERLAND</p>
<p><a href="http://soundcloud.com/josh-tree" target="_blank">Soundcloud</a> | <a href="http://www.myspace.com/djoshtree" target="_blank">Myspace</a></p>
<p>iTunes | Beatport | Whatpeopleplay | Amazon</p>
<p> </p><p>The musical creation of josh tree began not only for the love of music, but also that he was looking for a way to give shape to his own fantasy so he can impart mystical pictures and different worlds to public.</p>
<p>The mixture of his own faszination of repetitive synapse tickling music and great pleasure of technical equipment, little lights and buttons brings him to an intense affection for electronic music.</p>
<p>His dj career began early and became a long journey through different styles of music.</p>
<p>But he turns quickly to electronic music though because this style of music gives scope to imagination and fantasy as well as to creativity.</p>
<p>Through his audio engineering education Josh Tree could extend his knowledge about music production monumental and to gain experience in areas like scoring dance and theatre too.</p>
<p>Over the years Josh Tree could refine his technique of mixing music more and more and he could excite the audience with his not always easily digestible, often dark and mystic sounds.</p>
<p>The exploring of worlds of feelings like the deep chasms of our being and hidden desires have priority in his productions too. He enhances the experience of the tonal worlds, which rise like this, with a rich portion of rhythm for shaking and travel with.</p>
<p>During his producer work, Josh Tree fiddles particularly with the analogue modular synthesis and he is looking for a special sound experience, which betrayes you not only into dancing but also into being thought-provoking.</p><h3> </h3>
<h3>Releases</h3>
<p><strong>2013:</strong> <a href="../releases/dcr012">Balls (DCR012)</a></p></div><div class="ccm-spacer"></div></div></div></div>
