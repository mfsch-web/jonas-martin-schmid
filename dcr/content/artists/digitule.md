---
title: Digitule
soundcloud-id: 79221200
prelude: '<div id="ccm-layout-wrapper-2612" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><div id="blockStyle1561HauptbereichLayout1Cell1158" class=" ccm-block-styles" ><img border="0" class="ccm-image-block" alt="" src="../img/artists/Digitule.jpg"  width="400" height="319" /></div>'
---

</div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">DIGITULE [Producer/DJ]</span> </p>
<p>BASEL/SWITZERLAND</p>
<p><a href="http://soundcloud.com/digitule" target="_blank">Soundcloud</a> | <a href="http://www.facebook.com/digitule" target="_blank">Facebook</a> </p>
<p>iTunes | Beatport | Whatpeopleplay | Amazon (coming soon)<br /><br /></p><p>The young duo "Digitule" from Basel invites you to a world of deep techno and deephouse, beautiful melodies. With their DJ sets Digitule enchant the crowd for hours and gives you an unforgettable club experience. With their EP „24“, they show you, that they can keep up with the big ones. </p><h3>Releases</h3>
<p><strong>2015:</strong> <a href="../releases/dcr033">24 (DCR033)</a></p></div><div class="ccm-spacer"></div></div></div></div>
