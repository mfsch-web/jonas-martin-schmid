---
title: Dani Corbalan
soundcloud-id: 10966379
prelude: '<div id="ccm-layout-wrapper-2610" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-99-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-99-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:100%">&nbsp;</div><div class="ccm-spacer"></div></div></div></div><div id="ccm-layout-wrapper-2611" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-2" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-2 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><img border="0" class="ccm-image-block" alt="" src="../img/artists/Dani-Corbalan.jpg"  width="400" height="441" /><div id="blockStyle1134HauptbereichLayout2Cell1105" class=" ccm-block-styles" >'
---

</div></div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">DANI CORBALAN [Producer/DJ]</span> </p>
<p>BARCELONA/SPAIN</p>
<p><a href="https://soundcloud.com/danicorbalan" target="_blank">Soundcloud</a> | <a href="http://www.youtube.com/user/gat0x23" target="_blank">Youtube</a> | <a href="http://www.facebook.com/dani.corbalan28 " target="_blank">Facebook</a> | <a href="#interview">Interview</a></p>
<p><a href="https://itunes.apple.com/de/artist/dani-corbalan/id687039265" target="_blank">iTunes</a> | <a href="http://www.beatport.com/artist/dani-corbalan/352389" target="_blank">Beatport</a> | <a href="https://www.whatpeopleplay.com/?redirect=/artistdetails/Dani-Corbalan/id/49557" target="_blank">Whatpeopleplay</a> | <a href="http://www.amazon.de/s/ref=ntt_srch_drd_B00ESAG2ZU?ie=UTF8&amp;field-keywords=Dani%20Corbalan&amp;index=digital-music&amp;search-type=ss" target="_blank">Amazon</a></p><p>Dani Corbalan started to compose music when he was 11. With Impulse Tracker, he did around 100 songs in that period. In 1997, with 13 years, he won a spanish MODs contest, with his song "Dreams of Life", a relaxing song with amazing flute melody.</p>
<p>In 1998 he published his first disc with Up Tempo, a Hardcore label, called TEMPORIUM. One of the songs, The Slave, were published in a famous catalonian club compilation, XQUE.</p>
<p>In 2000, he started to use Cubase, Reason and other stuff, to make more quality music. In that period and until 2008 he did around 50 songs, with no luck, but he knows why: he was composing music without watching other artists, he was doing his own structure, and wasn't too much comercial. </p>
<p>From 2008 to 2010 he stopped composing because of that, but in 2010 he started again, with more quality, and listening to other artists.</p>
<p>Now he tries to do music specially for club. His prefered styles are Trance and Uplifting Trance, but he likes to mix that with House, Progressive and Minimal.</p>
<p>He loves guitars, pianos and strings, he always tries to create melodies that makes you feel things, that makes you remember that song, not just another comercial song.</p>
<p>In almost all his songs he always tries to make first and last minute for mixing it easy, that's very important for me cause I've been DJ too some time.</p><h3> </h3>
<h3>Releases</h3>
<p><strong>2014: </strong><a href="../releases/dcr022">Tear it down (DCR022)</a></p>
<p><strong>2013:</strong> <a href="../releases/dcr015">Back from the depths (DCR015)</a></p></div><div class="ccm-spacer"></div></div></div></div><p><p><span style="font-size: medium;"><strong><a name="interview"></a>Interview (12.10.2013)</strong></span></p>
<div><strong>How did you get in touch with electronic music?</strong></div>
<div> </div>
<div>When I was 8, I heard some "Makina" songs, a spanish style from 90's centered in Barcelona and Valencia, it was a mix of techno with trance melodies and house/hiphop vocals.</div>
<div> </div>
<div>That music was something incredible for me, that melodies, that rythms.. with 9 years old, all saturday nights I were waking up at 2 A.M. to record Makina sessions in the radio.</div>
<div> </div>
<div>I was in love with music after that period, forever.</div>
<div> </div>
<div>Some songs I remember that marked me: </div>
<div> </div>
<div>3 Elements - Dis Flute</div>
<div> </div>
<div>Ravers - Dehlia</div>
<div> </div>
<div>DSigual Vol.3</div>
<div> </div>
<div>Generis - Dreams</div>
<div> </div>
<div>Two Good - Happy Moment</div>
<div> </div>
<div><strong>Can you tell us a little bit about your release "Back from the depths"?</strong></div>
<div> </div>
<div>I started doing music in 1995 when I was 11, with no breaks until 2008, then lot of shit happened in my life and I stopped doing music.</div>
<div> </div>
<div>At the end of 2011 I started to do music again, all 2012 I've been sending my music to labels with no luck, so I was starting to lose my musician spirit.. until December of that year, when 1 label signed one of my songs.</div>
<div> </div>
<div>After that, in 2013 I started to do music seriously, every song have more quality than the last one, I learn something new everyday about producing music, mastering, etc..</div>
<div> </div>
<div>So now I feel like I'm back from the depths, from a dark hole, now I see the light in the musical world, I have a lot to offer, and I will work hard to make better music everyday.</div>
<div> </div>
<div><strong>How do you create your songs?</strong></div>
<div> </div>
<div>With a shit PC with shit Soundcard, old speakers and a pirate Cubase 5 xD, I try my best to create quality music, but I will need a better studio in the future. </div>
<div> </div>
<div>Sometimes when I'm really inspired doing a song, stupid PC reboots, or soundcard starts to crush my speakers, etc.. things that makes you crazy, specially when you are inspired.</div>
<div> </div>
<div>When I do a song I save it every 5 minutes xD.</div>
<div> </div>
<div>About my melodies, I don't know how to play any instrument, melodies are in my mind, I just paint notes manually in Cubase, maybe I'm crazy xD.</div>
<div> </div>
<div><strong>What is the most important thing for you in music?</strong></div>
<div> </div>
<div>Melodies, pianos, guitars, cellos, saxos.. I can't do a repetitive song, I know comercial songs must be repetitive, but I hate that, now I'm trying to create more comercial songs, but isn't easy for me.</div>
<div> </div>
<div>A song have to make you feel things, in a club any trash song can make you jump with alcohol, etc.. but if you listen that song in your house, with headphones, and you feel nothing, I did a bad work.</div>
<div> </div>
<div>After melodies, I like good rythms, deep basses and strong kicks.</div>
<div> </div>
<div>Vocals.. in fact, I don't care about letters, I see vocals as another instrument, I don't care if an acapella is in english, spanish, russian or chinese, I only care if that vocal fits the melodies, or the melody is perfect for that vocal, for me, that's the spirit of music, not the letters.</div>
<div> </div>
<div><strong>What are your plans for 2014?</strong></div>
<div> </div>
<div><span>1.- No more Trance, was my preferred style, but it's totally dead, only famous Trance Dj's can publish Trance, new producers can't, labels ignore them. Famous control all, and any trash song they publish gets #1, it's really sad cause I loved that style, but it's dead.</span></div>
<div> </div>
<div>2.- Publishing a release with a big label, I don't care if House, Deep, Tech or Progressive, but would be a dream to publish something with a big label like Spinnin' Records or Axtone.</div>
<div> </div>
<div>3.- Getting a better studio.</div>
<div> </div>
<div>4.- Start my DJ career, but for that first I need to do point 3 xD, anyway it's really easy nowadays to be a DJ, Pioneer stuff does all, you only need to press play :P </div>
<div> </div>
<div>5.- In 2013, I published 5 releases and if all goes ok I'm gonna publish 4 more releases including Back from the Depths, I hope in 2014 I double that number at least.</div>
<div> </div>
<div>6.- It will hurt, but I will have to do comercial, comercial and comercial music, to get a name in that world, so I will listen a lot of top10 lists, and get ideas from there.</div>
<p><strong><br /></strong></p></p>
