---
title: K-N-I-G-H-T
soundcloud-id: 296881763
prelude: '<div id="ccm-layout-wrapper-2616" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><img border="0" class="ccm-image-block" alt="" src="../img/artists/K-N-I-G-H-T.jpg"  width="400" height="400" /><div id="blockStyle1608HauptbereichLayout1Cell1162" class=" ccm-block-styles" >'
---

</div></div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">K-N-I-G-H-T [Producer/DJ]</span> </p>
<p>LONDON/ENGLAND</p>
<p><a href="http://soundcloud.com/k-n-i-g-h-tofficial" target="_blank">Soundcloud</a> </p>
<p>iTunes | Beatport | Whatpeopleplay | Amazon<br /><br /></p><p>K-N-I-G-H-T is an exciting techno producer based in London. Not content to settle on a particular sound, preferring instead to deliver clean and fresh productions that pack a punch while constantly evolving for the listener’s interest, K-N-I-G-H-T tracks offer something a little bit different from the norm.<br class="yiv2571856577" clear="none" />Believing in the opportunity for flexible self-expression that techno allows, K-N-I-G-H-T's spot-on mixing and production skills provide the base for tracks both accessible yet unique. With EP releases out on 2RB Records, Germany; 7 Stars Music, Netherlands; Deepsessions Recordings Greece; Euphoria Sounds, Australia, and multiple remixes under his belt, K-N-I-G-H-T is making his mark.</p><p> </p>
<h3>Releases</h3>
<p><strong>2015:</strong> <a href="../releases/dcr034">Moon (DCR034)</a></p></div><div class="ccm-spacer"></div></div></div></div>
