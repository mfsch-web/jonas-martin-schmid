---
title: B.free
soundcloud-id: 405340
prelude: '<div id="ccm-layout-wrapper-2608" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><div id="blockStyle1205HauptbereichLayout1Cell1128" class=" ccm-block-styles" ><img border="0" class="ccm-image-block" alt="" src="../img/artists/B.free.jpg"  width="400" height="400" /></div><div id="blockStyle1208HauptbereichLayout1Cell1129" class=" ccm-block-styles" >'
---

</div></div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">BEAT FREI [Producer/Liveact]</span></p>
<p>BASEL/SWITZERLAND</p>
<p><a href="http://soundcloud.com/beat-frei" target="_blank">Soundcloud</a> | <a href="http://www.bfree.ch/" target="_blank">Official Website</a> | <a href="http://mx3.ch/artist/bfree" target="_blank">MX3</a></p>
<p>iTunes | Beatport | Whatpeopleplay | Amazon </p>
<p><a href="#interview">Interview</a></p>
<p> </p><p>Beat Frei alias B.free, born in Basel, Switzerland, has been an autodidact from the start. Despite the fact that he learned to play the piano, the accordion and the famour Swiss Basler drum when he was a child, he never wanted to study music at an academy. As a teenager, he was travelling around the world to find himself and his music, which resulted in being part of different bands, playing all kinds of music.</p>
<p>With the rise of the personal computer in 1989 and the possibility to create, arrange and sample music, Beat got electrified and totally dedicated to the new medium.</p>
<p>Now, he had the possibilities to create a world of music. He put together sounds in new, different and unusual ways just to see what's happens.</p>
<p>In 1995 Beat started an own record label for minimal house music, which was intended for DJs, created soundscapes for paintings and sound installations, always driven by innovation and the commitment to create unusual new music and sounds.</p>
<p>1998, he gratuated in sound engineering and started to work for the Theater Basel. </p>
<p>During this time, Beat created a lot of different pieces of music for the theater and tv, but also for solo dancers like Sandra Schöll. Besides, he was composing music for the DV8 Phyisical Theater and launched his first three Cds</p><h3> </h3>
<h3>Releases</h3>
<p><strong>2014:</strong> <a href="../releases/dcr025">Schleudertrauma (DCR025)</a></p>
<p><strong>2013: </strong><a href="../releases/dcr017">2014[Wut] (DCR017)</a></p></div><div class="ccm-spacer"></div></div></div></div><p><p><span style="font-size: medium;"><a name="interview"></a>Interview (18.03.2014)</span></p>
<p><strong>1. How did you get in touch with electronic music?</strong><br />on X-mas 1989 my wife gave me a Atari 1040 for x-mas and said, "here is your new band"<br /><strong></strong></p>
<p><strong>2. How you would describe your release „2014 [Wut]?</strong><br />this is a 14 minute soundtrack from my live in 4 parts<br /><strong></strong></p>
<p><strong>3. You produce music for theatre and movies too. What is the difference between producing for a release and producing for theatre or movies?</strong><br />music for theatre, film or ballet is always a coproduction and a teamwork and you work with very creative people <br />together and the inspiration is a real enrichment factor.<br />i also love to be in the studio by my self and turn the knobs or playing violin with my E-Drum;)<br />i don’t know how it is when somebody would give me money to make a hole album, <br />maybe it will be a concept album instead of a collection of many ideas.</p>
<p><strong>4. You are a member of the group „random kings“. Can you tell us a little bit about „random kings“?</strong><br />The randomkings are a collective of  4 regulars and some guests. <br />we never rehears, we meet at the gigs, no computers are alowed only step sequencers, rhythm machines and synths, <br />connected via midi or sync.<br />we never play under 3 hours, with exceptions, the longest gig was 8 hours on a lan party<br /><br /><strong>4. How do you create your songs?</strong><br />First is a idea, par example i heard  from the fibboncci code, By definition, the first two numbers in the Fibonacci sequence are 1 and 1, or 0 and 1, depending on the chosen starting point of the sequence, and each subsequent number is the sum of the previous two. so i try to play it on the drums or transcribe it in to notes and so on.</p>
<p><strong>5. What is the most important thing for you in music?</strong><br />it is a complete different world big as the universe and it is a language without words.<br />b.t.w. i was showing a track to my wife and she said not really happy „this makes me real angry“.  I called the track WUT, that is the translation of the word angry in german:)</p>
<p><strong>6. What are your plans for 2014?</strong> <br />there are no plans really , i like to go to helsinki to the premiere from a dance performance where i made the music im march and i am looking forward to the release from WUT.</p>
<p><strong>Thanks!</strong></p></p>
