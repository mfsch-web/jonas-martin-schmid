---
title: Hansgod
soundcloud-id: 1284791
prelude: '<div id="ccm-layout-wrapper-2614" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><div id="blockStyle1140HauptbereichLayout1Cell1122" class=" ccm-block-styles" ><img border="0" class="ccm-image-block" alt="" src="../img/artists/Hansgod.jpg"  width="400" height="388" /></div><div id="blockStyle1646HauptbereichLayout1Cell1167" class=" ccm-block-styles" >'
---

</div></div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">HANSGOD [Producer/DJ/Liveact]</span></p>
<p>RENNES/FRANCE</p>
<p><a href="http://soundcloud.com/hansgod" target="_blank">Soundcloud</a></p>
<p><a href="https://itunes.apple.com/at/artist/hansgod/id739398259" target="_blank">iTunes</a> | <a href="http://www.beatport.com/artist/hansgod/372086">Beatport</a> | <a href="https://www.whatpeopleplay.com/?redirect=/artistdetails/Hansgod/id/49739" target="_blank">Whatpeopleplay</a> | <a href="http://www.amazon.de/s/ref=ntt_srch_drd_B00GID77E0?ie=UTF8&amp;field-keywords=Hansgod&amp;index=digital-music&amp;search-type=ss" target="_blank">Amazon</a> </p>
<p> </p><p>Hansgod is a french producer dj/live/act who evolved with electronic music .His breakbeat and hip-hop roots made him reach house music and later techno music.He began mixing at 16 years old and made his first step on a sequencer at 21.Today  he wants to stop making melodies to work on tech-house and minimal  sound objects ...</p>
<p>He is resident of a new but well known underground place, the after bar in western France. </p><h3> </h3>
<h3>Releases</h3>
<p><strong>2015: </strong><a href="../releases/dcr029">Pulp (DCR029)</a></p>
<p><strong>2014: </strong><a href="../releases/dcr019">Afroots (DCR019)</a></p>
<p><strong>2013: </strong><a href="../releases/dcr016">Ice cream (DCR016)</a></p></div><div class="ccm-spacer"></div></div></div></div><p><p><strong><span style="font-size: medium;">Interview (12.10.2013)</span></strong></p>
<p><strong>How did you get in touch with electronic music?</strong></p>
<p>At the age of eleven ,i think it was in a motel room ,on a radio i did'nt know the name it was a flash for me. But all my life when i was a child ,i've been searching electronic music in all songs i listened ,synths etc. I didn't realised  that it was just synth sounds that i was looking for.</p>
<p><strong>Can you tell us a little bit about your release "ice cream"?</strong></p>
<p><strong></strong>It's a dancefloor minimalistic EP whith strange spaces like you said.</p>
<p><strong>How do you create your songs?</strong></p>
<div>I begin with the kick and i build a rythmic, i'm on ableton.</div>
<div> </div>
<div><strong>How you play live?</strong></div>
<div> </div>
<div>I play with dubby effects like delays ,reverbs. i create surprises but i let the beat making its job. sometimes the beat is broken but just with the kick, i don't like clap rollin etc</div>
<div> </div>
<div><strong>What is the most important thing for you in music?</strong></div>
<div><br />Now when i do techno especially, i don't want to use melody, i just want rythm artefacts and a mad bass.</div>
<div> </div>
<div><strong>What are your plans for 2014?</strong></div>
<div>
<div> </div>
<div>Making good music, minimalistic tech house and a little bit of progressive house.</div>
</div></p>
