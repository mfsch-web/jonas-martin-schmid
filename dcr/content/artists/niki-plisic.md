---
title: Niki Plisic
soundcloud-id: 113045736
prelude: '<div id="ccm-layout-wrapper-2729" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><img border="0" class="ccm-image-block" alt="" src="../img/artists/Niki-Plisic.jpg"  width="400" height="400" /><div id="blockStyle1611HauptbereichLayout1Cell1163" class=" ccm-block-styles" >'
---

</div></div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">NIKI PLISIC [Producer]</span> </p>
<p>RIJEKA/CROATIA</p>
<p><a href="http://soundcloud.com/niki-plisic" target="_blank">Soundcloud</a> | <a href="http://www.facebook.com/niki7garcia" target="_blank">Facebook</a></p>
<p>iTunes | Beatport | Whatpeopleplay | Amazon<br /><br /></p><p>Niki Plisic was born 1988 in Rijeka/Croatia.Music is very important part of his life.In the begining he listened different sorts of music like hip hop to techno,which he started listening on cassete player's with his older brother.It were giants like (Space Dj'z,Dave Clarke,Richie Hawtin,,Prodigy,Umek,Jeff Mills and many others that were influential...).With 16 years he started using a pc program called Fruity Loops and his first music producing were hip hop beats were he learned how to use Fruity Loops.Later he tried making electronic music.He has a wish to create music as a way of life, and not as a job or hobby.</p></div><div class="ccm-spacer"></div></div></div></div>
