---
title: Outpost of Progress
soundcloud-id: 139013315
prelude: '<div id="ccm-layout-wrapper-2688" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><img border="0" class="ccm-image-block" alt="" src="../img/artists/Outpost-of-Progress.jpg" width="400" height="272" />'
---

</div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">OUTPOST OF PROGRESS [Producer/Liveact]</span> </p>
<p>FLORENCE/ITALY</p>
<p><a href="https://soundcloud.com/outpost-of-progress" target="_blank">Soundcloud</a> | <a href="%20https://www.facebook.com/outpostofprogress" target="_blank">Facebook</a> | <a href="https://twitter.com/O_OProgress" target="_blank">Twitter</a></p>
<p>iTunes | Beatport | Whatpeopleplay | Amazon<br /><br /></p><p>Outpost of Progress is an audio-visual alternative electronic duo from Florence (Italy). Conceived in 2014 out of the collaboration between music producer Riccardo Pinzuti and singer/songwriter Max Bindi. The name of the project was inspired by the homonymous short story written by the author Joseph Conrad, to underline the explorative quality of the music and blends divergent sources of electronic sound with hypnotic melodies. The  objective of Outpost of Progress’s music aims to transcend a definite musical dimension or rather to intrude between the fluid boundaries of specific genres. Their live shows are characterized by the aid of visual devices, in the attempt to create a fully emotional and rewarding multi -media experience.</p></div><div class="ccm-spacer"></div></div></div></div>
