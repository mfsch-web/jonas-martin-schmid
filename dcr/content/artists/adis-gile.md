---
title: Adis Gile
soundcloud-id: 347936
prelude: '<div id="ccm-layout-wrapper-2296" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-34-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-34-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:100%">&nbsp;</div><div class="ccm-spacer"></div></div></div></div><div id="ccm-layout-wrapper-2297" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-73-2" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-2 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-73-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:47%"><img border="0" class="ccm-image-block" alt="" src="../img/artists/Adis-Gile.jpg"  width="400" height="353" /><div id="blockStyle1316HauptbereichLayout2Cell153" class=" ccm-block-styles" >'
---

</div></div><div class="ccm-layout-73-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:52.99%"><p><span class="ueberschr2">ADIS GILE [Producer/DJ]</span> </p>
<p>BOSNIA HERZEGOWINA</p>
<p><a href="http://soundcloud.com/adis_g" target="_blank">Soundcloud</a> | <a href="http://www.facebook.com/adis.gile" target="_blank">Facebook</a> | <a href="http://www.residentadvisor.net/dj/adisgile" target="_blank">Resident Advisor</a></p>
<p><a href="https://itunes.apple.com/de/artist/adis-gile/id563680486" target="_blank">iTunes</a> | <a href="http://www.beatport.com/artist/adis-gile/158560" target="_blank">Beatport</a> | <a href="http://www.whatpeopleplay.com/?redirect=/artistdetails/Adis-Gile/id/41061" target="_blank">Whatpeopleplay</a> | <a href="http://www.amazon.de/s/ref=ntt_srch_drd_B009E4BEAI?ie=UTF8&amp;field-keywords=Adis%20Gile&amp;index=digital-music&amp;search-type=ss" target="_blank">Amazon</a><br /><br /></p><p>„Music is not just a way of life. It is the way of life. You'll find yourself dancing (in the mind or with the body) to the most unusual sounds. Find a beat and you'll be satisfied.“</p>
<p>Adis Gile is an electronic music producer and DJ from Bosnia and Herzegowina. His main style is Deep/Techhouse, but he also likes the sound of a good funky techno track.</p>
<p>Adis Gile started to professionaly DJ around 2008; He plays rythm and solo guitar in an acoustic cover band called Egentis.</p>
<p>In 2010 Adis Gile recorded a techhouse mix called SOMMER SONNE FRECHE GOEHREN which won him a place at the electronic music conference in EXIT - which is the biggest and most know music festival in south-east Europe.</p>
<p>In 2011 he worked as a rezident DJ in charge for POP/HIPHOP/HOUSE/TECHNO in „DISKOTEKA CAMEL“ – one of the biggest clubs in the Balkans which is visited by nearly 2000 people every night.</p>
<p>His first release was a techhouse remix for AS_Brain - La Morena on a Colombian record label called Dual Records.</p>
<p>In 2012. Adis Gile continues to work on his musical journey.</p><h3> </h3>
<h3>Releases</h3>
<p><strong>2012:</strong> <a href="../releases/dcr011">The city of mine (DCR011)</a></p></div><div class="ccm-spacer"></div></div></div></div>
