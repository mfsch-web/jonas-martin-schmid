---
title: Aurel den Bossa
soundcloud-id: 11353674
prelude: '<div id="ccm-layout-wrapper-2607" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><img border="0" class="ccm-image-block" alt="" src="../img/artists/Aurel-den-Bossa.jpg" width="400" height="400" /><div id="blockStyle1496HauptbereichLayout1Cell1152" class=" ccm-block-styles" >'
---

</div></div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">AUREL DEN BOSSA [Producer/DJ]</span> </p>
<p>RENNES/FRANCE</p>
<p><a href="https://soundcloud.com/aurel-den-bossa" target="_blank">Soundcloud</a> | <a href="https://www.facebook.com/aurel.denbossa" target="_blank">Facebook</a> </p>
<p>iTunes | Beatport | Whatpeopleplay | Amazon<br /><br /></p><p><span data-reactid=".v.$mid=11414169805045=2bac7ee9bf5d0181e28.2:0.0.0.0.0.0.$end:0:$10:0">Aurel den Bossa is a french techno to deep house producer. He loves beat with beautiful melodies. As he was 17 years old he bought his first turntables. 2011 he produced his first tracks on logic.</span><br data-reactid=".v.$mid=11414169805045=2bac7ee9bf5d0181e28.2:0.0.0.0.0.0.$end:0:$11:0" /><span data-reactid=".v.$mid=11414169805045=2bac7ee9bf5d0181e28.2:0.0.0.0.0.0.$end:0:$12:0">To be continued ...</span></p><p><strong>Releases:</strong></p>
<p>2015:<a href="../releases/dcr31"> First Time (DCR031)</a></p></div><div class="ccm-spacer"></div></div></div></div>
