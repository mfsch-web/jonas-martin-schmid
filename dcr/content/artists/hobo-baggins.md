---
title: Hobo Baggins
soundcloud-id: 93432053
prelude: '<div id="ccm-layout-wrapper-2615" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><img border="0" class="ccm-image-block" alt="" src="../img/artists/Hobo-Baggins.jpg" width="400" height="400" /><div id="blockStyle1459HauptbereichLayout1Cell1151" class=" ccm-block-styles" >'
---

</div></div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">HOBO BAGGINS [Producer/Liveact]</span> </p>
<p>STUTTGART/GERMANY</p>
<p><a href="https://soundcloud.com/hobo-baggins" target="_blank">Soundcloud</a> | <a href="https://www.facebook.com/Hobobagginsmusic" target="_blank">Facebook</a></p>
<p>iTunes | Beatport | Whatpeopleplay | Amazon<br /><br /></p><p><span>Hobo Baggins, a.k.a. Timo Grau is a music producer from near Stuttgart, Germany. He took his first humble steps towards music production at the age of eight. Ever since he has been writing his own songs, sometimes also together with several bands or with his brother, with whom he releases music on Dirty Cat Records under the pseudonym "Buttheads". Having studied biology for several years, he tries to give his music this kind of vivid sound that nature produces. This occasionally results in hour-long sound fiddeling and recording sessions and staying awake when normal people sleep. Unfortunately beeing very bad in all promotional things, he is lucky to have Jonas and Dirty Cat Records for all that stuff.</span></p><p> </p>
<h3>Releases</h3>
<p><strong>2015:</strong> <a href="../releases/dcr028">Relikte historischer Nutzungen (DCR028)</a></p></div><div class="ccm-spacer"></div></div></div></div>
