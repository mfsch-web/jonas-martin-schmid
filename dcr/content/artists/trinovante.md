---
title: TrinoVante
soundcloud-id: 43562731
prelude: '<div id="ccm-layout-wrapper-2620" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><div id="blockStyle1534HauptbereichLayout1Cell1155" class=" ccm-block-styles" ><img border="0" class="ccm-image-block" alt="" src="../img/artists/TrinoVante.jpg"  width="400" height="400" /></div>'
---

</div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%"><p><span class="ueberschr2">TRINOVANTE [Producer]</span> </p>
<p>WICKHAM BISHOPS/UK</p>
<p><a href="https://soundcloud.com/trinovante" target="_blank">Soundcloud</a> | Facebook | <a href="https://twitter.com/mikaelbaker1" target="_blank">Twitter</a></p>
<p><a href="https://itunes.apple.com/de/artist/trinovante/id912427634">iTunes</a> | Beatport | Whatpeopleplay | Amazon<br /><br /></p><div>I have been experimenting with Electronic Music Production since studying music technology at University from 07-09.  I love to produce Deep House, Liquid Dubstep and Drum 'n' Bass.  I started to produce my own tracks, just for personal use, while I was at the gym or doing exercise. </div>
<div>However, recently my passion and love for producing my own music has pushed me to take things more seriously.  I have now been fortunate enough to release music, from all of my adored genres and styles of music production, with record labels.</div>
<div>I still have a lot to learn, but I am happy with my current progress.  Music is what you make it ..... there is no right or wrong way.</div><p> </p>
<h3>Releases</h3>
<p><strong>2014:</strong> <a href="../releases/dcr030">Pathetic Fallacy - Best Of Dirty Cat Records Vol. 2 (DCR030)</a></p></div><div class="ccm-spacer"></div></div></div></div>
