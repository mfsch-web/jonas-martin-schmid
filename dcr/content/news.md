---
title: NEWS
---

<div id="ccm-layout-wrapper-2746" class="ccm-layout-wrapper">
<div id="ccm-layout-hauptbereich-107-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 ">
<div class="ccm-layout-row ccm-layout-row-1">
<div class="ccm-layout-107-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:100%">

<p><span class="ueberschr">CURRENT NEWS</span></p>
<p><strong>11.09.2015: DCR039 Adis G - Bromazepam</strong></p>
<p>Adis G - Bromazepam is out now including remixes by Hobo Baggins, CUSCINO, Jan Ploetzlich and esc-laboratory </p>
<p><strong>28.08.2015: DCR038 Dani Corbalan - Cordobesa</strong></p>
<p>Dani Corbalan is back with a beautiful EP including 4 tracks for the dance floor.  </p>
<p><strong>21.08.2015: DCR037 Outpost of Progress - Wildcard</strong></p>
<p>We are happy to announce our new artist duo Outpost of Progress. They release their first EP "Wildcard" today. Welcome on board. </p>
<p> <strong> </strong></p>
<p> </p>

<table border="0" cellspacing="0" cellpadding="10"><tbody>

<tr>
  <td style="width: 300px;" valign="top"><p><span class="ueberschr"> NEW RELEASES </span></p></td>
  <td style="width: 300px;" valign="top"> </td>
  <td style="width: 300px;" align="center" valign="top"><p><span class="ueberschr"> UPCOMING RELEASES </span></p></td>
</tr>

<tr>
  <td valign="top">
    <p><a href="releases/dcr038"><img src="img/releases/Dani_Corbalan_-_Cordobesa_200.jpg" alt="Dani Corbalan - Cordobesa 200.jpg" width="200" height="200" /></a></p>
    <p><span class="genre">Dani Corbalan - Cordobesa (DCR038)<br /></span></p>
    <p><strong>House/Soul/Funk</strong></p>
    <p>Funky house beats, sexy saxophone, flute and trumpet tunes and driving guitar sounds. Cordobesa is a beautiful house track with Spanish flair. Crispy house beats, driving trumpet melodies, and dreamy guitar sounds, Spanish vibes for the  dancefloor.<br /><a href="releases/dcr038">&gt; More Informations</a></p>
    <p><strong>Omar Cito Perez (Pacha recordings)</strong> - I said it very early longtime ago... Dani Corbalan is an artist to keep an eye on. His work is always quality wheter it be club or deep. Great rework on Guivosa.</p>
    <p><strong>CUSCINO</strong> - (Fashion Sells Musiq, iHeart Radio) Great summer vibes...need to do a remix EP for "Lonely" ;-) I'll get at least one of these on my FutureSound radio show as well on iHeartRadio. Cheers! </p>
  </td>
  <td valign="top">
    <p><a href="releases/dcr039"><img src="img/releases/Adis_G_-_Bromazepam_200.jpg" alt="Adis G - Bromazepam 200.jpg" width="200" height="200" /></a></p>
    <p><span class="genre">Adis G - Bromazepam (DCR039)<br /></span></p>
    <p><strong>Tech-House/Deep House/Techno</strong></p>
    <p>Adis G is back with a bomb for the dancefloor, a tech-house track at its finest, including remixes by CUSCINO, Hobo Baggins, Jan Plötzlich and ESC Laboratory. Once again, a nice package of electronic music.<br /><a href="releases/dcr039">&gt; More Informations</a></p>
    <p><strong>XLII</strong> - Dope. Space travel on crack. </p>
    <p><strong>Andrew Consoli</strong> - Cool Ep, will try original mix! </p>
    <p><strong>koma</strong> - Cool rmxs, but original is my fave </p>
    <p><strong>Damaged Man</strong> - Nice Release!Full Support!</p>
    <p><strong>Matt Handy</strong> - Original mix is nice!! Thanks </p>
  </td>
  <td align="center" valign="top">
  <p> </p>
  </td>
</tr>

<tr>
  <td valign="top">
    <p><strong>Listen to and buy the release <strong>at our Shop:</strong></strong></p>
    <iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=4285110434/size=large/bgcol=333333/linkcol=ffffff/tracklist=false/artwork=none/transparent=true/">&amp;amp;lt;a href="http://dirtycatrecords.bandcamp.com/album/cordobesa" data-mce-href="http://dirtycatrecords.bandcamp.com/album/cordobesa"&amp;amp;gt;Cordobesa by Dani Corbalan&amp;amp;lt;/a&amp;amp;gt;</iframe>
  </td>
  <td valign="top">
    <p><strong>Listen to and buy the release at our Shop:</strong></p>
    <iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=840863889/size=large/bgcol=333333/linkcol=ffffff/tracklist=false/artwork=none/transparent=true/">&lt;a href="http://dirtycatrecords.bandcamp.com/album/bromazepam" data-mce-href="http://dirtycatrecords.bandcamp.com/album/bromazepam"&gt;Bromazepam by Adis G&lt;/a&gt;</iframe>
  </td>
  <td align="left" valign="top"> </td>
</tr>

</tbody></table>


<p><span class="ueberschr"> LATEST RELEASES </span></p>
<table border="0" cellpadding="10"><tbody>
  <tr>
    <td><a href="releases/dcr037"><img src="img/releases/DCR037_Outpost_of_Progress_-_Wildcard_200.jpg" alt="DCR037 Outpost of Progress - Wildcard 200.jpg" width="200" height="200" /></a></td>
    <td><a href="releases/dcr036"><img src="img/releases/DCR036_Aurel_den_Bossa_-_Oups_200.jpg" alt="DCR036 Aurel den Bossa - Oups! 200.jpg" width="200" height="200" /></a></td>
    <td><a href="releases/dcr035"><img src="img/releases/Niki_Plisic_-_Night_Story_200.jpg" alt="Niki Plisic - Night Story 200.jpg" width="200" height="200" /></a></td>
    <td><a href="releases/dcr034"><img src="img/releases/DCR034_K-N-I-G-H-T_-_Moon_200.jpg" alt="DCR034 K-N-I-G-H-T - Moon 200.jpg" width="200" height="200" /></a></td>
  </tr>
</tbody></table>

<p> </p>
<p> </p>

<p><span class="ueberschr"> FOLLOW US </span></p>

</div>

<div class="ccm-spacer"></div>

</div></div></div>

<div id="ccm-layout-wrapper-2745" class="ccm-layout-wrapper">
<div id="ccm-layout-hauptbereich-106-2" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-2 ">
<div class="ccm-layout-row ccm-layout-row-1">
<div class="ccm-layout-106-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:50%">

<div id="blockStyle1259HauptbereichLayout2Cell1135" class=" ccm-block-styles" >

<div id="HTMLBlock1259" class="HTMLBlock">
<a class="twitter-timeline" href="https://twitter.com/dirtycatrecords" data-widget-id="354910850349424640">Tweets von @dirtycatrecords</a>
<script type="text/javascript">// <![CDATA[
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
// ]]></script></div></div></div><div class="ccm-layout-106-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:49.99%">
<div id="HTMLBlock1257" class="HTMLBlock">
<script src="http://connect.facebook.net/de_DE/all.js#xfbml=1"></script><fb:like-box href="http://www.facebook.com/dirtycatrecords" width="292" height="200" show_faces="true" stream="false" header="false"></fb:like-box></div></div><div class="ccm-spacer"></div></div></div></div>
