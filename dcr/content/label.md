---
title: LABEL
---

<div id="ccm-layout-wrapper-2752" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-98-2" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-2 "><div class="ccm-layout-row ccm-layout-row-1">

<div class="ccm-layout-98-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:48%"><div id="blockStyle1744HauptbereichLayout2Cell1101" class=" ccm-block-styles" >
  <p><span class="ueberschr2">DIRTY CAT RECORDS</span></p>
  <p><a href="http://soundcloud.com/dirtycatrecords" target="_blank">SOUNDCLOUD</a> | <a href="http://www.facebook.com/pages/Dirty-Cat-Records/148681418522231" target="_blank">FACEBOOK</a> | <a href="http://twitter.com/dirtycatrecords" target="_blank">TWITTER</a> | <a href="http://www.youtube.com/user/dirtycatrecordsvideo/videos" target="_blank">YOUTUBE</a> |<br /><a href="http://www.residentadvisor.net/record-label.aspx?id=7072" target="_blank">RESIDENTADVISOR</a> | <a href="http://www.lastfm.de/label/Dirty+Cat+Records" target="_blank">LAST.FM</a></p>
  <p><a href="http://www.beatport.com/label/dirty-cat-records/21033" target="_blank">BEATPORT</a> | <a href="http://www.amazon.de/s/ref=nb_sb_noss?__mk_de_DE=%C5M%C5Z%D5%D1&amp;url=search-alias%3Ddigital-music&amp;field-keywords=dirty+cat+records&amp;x=0&amp;y=0" target="_blank">AMAZON</a> | <a href="http://www.whatpeopleplay.com/labeldetails/null/id/000004210" target="_blank">WHATPEOPLEPLAY</a> | <a href="https://www.traxsource.com/label/14447/dirty-cat-records" target="_blank">TRAXSOURCE</a> | <a href="http://www.junodownload.com/labels/Dirty+Cat/releases/" target="_blank">JUNO</a></p>
  <p><strong> </strong></p>
  <p><strong>Dirty Cat Records stands for high quality electronic music that moves.</strong> </p>
  <p>It is a plattform for highly ambitious artists, a global network of creative relationships. Nowadays, a lot of artists are put under pressure to adapt and produce music for the masses. Dirty Cat Records aims to support artists with a strong personality, who want to choose their own path, not letting themselves be influenced only by mainstream. </p>
  <p><strong>Artistic freedom is essential for creative music production.</strong></p>
  <p><em>“With my music, I want to touch and stir people, I want to take them on a journey. My feelings, my moods, I want to capture and communicate them in the form of music. Nobody should tell me which music to make, I want to create something of my own.” (Dirty Cat 2011) </em></p>
  <p><strong>Genres: all kind of electronic music.</strong></p>
  <p> </p>
  <p><strong>Friends:</strong></p>
  <p><a href="http://uhumusic.ch/" target="_blank">UHU Records (Basel/Swiss)</a></p>
  <p><a href="https://www.facebook.com/filterlabel" target="_blank">Filter Label (Mazedonia)</a></p>
  <p><a href="http://www.globalvortexrecords.com" target="_blank">Globel Vortex Records</a></p>
  <p><a href="http://www.trepok.com" target="_blank">Trepok Records (Zürich/Swiss)</a></p>
  <p> </p>
</div></div>


<div class="ccm-layout-98-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:51.99%">

<div id="blockStyle974HauptbereichLayout2Cell2100" class=" ccm-block-styles" >
<iframe width="100%" height="300" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/6721806&amp;color=990000&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
</div>

<div id="HTMLBlock970" class="HTMLBlock"><div class="beatport" align="center" style="border:0px; background:transparent; padding:10px;">
<map name="bottomlinks"><area href="http://www.beatport.com" alt="Go to Beatport.com" coords="0,0,225,50" target="_blank"/>
<area href="http://www.beatport.com/viralPlayer/relay?playerId=2195207" alt="Get These Tracks" coords="237,12,332,38" target="_blank"/>
<area href="http://player.beatport.com/?playerId=2195207" alt="Add This Player" coords="332,12,422,38" target="_blank"/>
</map>
<img src="http://geo-media.beatport.com/images/beatport/viralPlayer/top.gif" style="display:block; border:none;" usemap="#bottomlinks"/>
<object type="application/x-shockwave-flash" data="https://www.beatport.com/CDN/swf/beatportplayer.swf" height="264" width="442" style="display:block;" align="center">
<param name="movie" value="https://www.beatport.com/CDN/swf/beatportplayer.swf"/>
<param name="allownetworking" value="internal"/>
<param name="allowScriptAccess" value="never"/>
<param name="enableJSURL" value="false"/>
<param name="enableHREF" value="false"/>
<param name="saveEmbedTags" value="true"/>
<param name="flashvars" value="bpCfgPath=http://www.beatport.com/en-US/xml/gui/swf/configuration/3&playerId=2195207&autoplay=0&volume=80"/>
<param name="loop" value="false"/>
<param name="menu" value="false"/>
<param name="quality" value="high"/>
<param name="salign" value="lt"/>
<param name="scale" value="noscale"/>
</object>
</div></div>

</div>

<div class="ccm-spacer"></div>

</div></div></div><div id="ccm-layout-wrapper-2751" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-14-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-14-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:100%">&nbsp;</div><div class="ccm-spacer"></div></div></div></div>
