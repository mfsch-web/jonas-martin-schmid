---
title: CONTACT
---

<p>
  <p><em> info[at]dirtycatrecords.com</em><br /> <br /> <strong>Booking:</strong><br />If you are interested for an artist for playing live, sent an e-mail with informations and contact infos to: <br /> <em>booking[at]dirtycatrecords.com</em><br /> <br /><strong>Demo:</strong><br />If you want to sent us your demo, please use something like <a href="http://www.sendspace.com/" target="_blank">sendspace</a> and sent us an e-mail with the download link, your biography, one photo and contact infos to: <em>demo[at]dirtycatrecords.com</em></p>
  <p>We will answer as soon as possible.</p>
  <p><strong>We don't accept e-mails with attachements!</strong></p>
  <p><strong>Promos:</strong><br />For Promos use <em>promos[at]dirtycatrecords.com</em></p>
  <p> </p>
</p>
