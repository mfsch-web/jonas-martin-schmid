---
title: DCR010
---

<div id="ccm-layout-wrapper-2406" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-69-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-69-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:49%"><div id="blockStyle814HauptbereichLayout1Cell146" class=" ccm-block-styles" >
<img border="0" class="ccm-image-block" alt="" src="../img/releases/DCR010_cover.jpg"  width="400" height="400" /></div>
<div id="HTMLBlock1409" class="HTMLBlock">
<iframe style="border: 0; width: 400px; height: 120px;" src="http://bandcamp.com/EmbeddedPlayer/album=363193037/size=medium/bgcol=333333/linkcol=ffffff/transparent=true/artwork=none/" seamless><a href="http://buttheads.bandcamp.com/album/st-r-ophonie-remixes">Stéréophonie Remixes by Buttheads</a></iframe></div></div><div class="ccm-layout-69-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:50.99%"><p><span class="ueberschr2">[DCR010] BUTTHEADS - STÉRÉOPHONIE REMIXES</span><br /> <span class="genre">Electro/House/Dicso | 30.07.2012</span></p>
<p>A little EP with two remixes made by SWEAR and Buttheads themselves as a free download.</p>
<p><strong>Tracklisting:</strong><br /> 01 La Stéréophonie (SWEAR Remix) (05.01) <br />2) La Stéréophonie (Buttheads you can not call this remix Remix)(2:34)</p></div><div class="ccm-spacer"></div></div></div></div>
