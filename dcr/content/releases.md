---
title: RELEASES
releases:
  - id: 039
    title: 'Adis G - Bromazepam'
    genre: 'Tech-house/Deep House/Electronica'
    date: '11.09.2015'
    description: 'Adis G is back with a bomb for the dancefloor, a tech-house track at its finest, including remixes by CUSCINO, Hobo Baggins, Jan Plötzlich and ESC Laboratory. Once again, a nice package of electronic music.'
  - id: 038
    title: 'Dani Corbalan - Cordobesa'
    genre: 'House/Soul/Funk'
    date: '2015-08-28'
    description: 'Funky house beats, sexy saxophone, flute and trumpet tunes and driving guitar sounds. Dani Corbalan is back with three new house tracks for the dancefloor and a nice rework of Guivosa.'
  - id: 037
    title: 'Outpost of Progress - Wildcard'
    genre: 'Alternative/Electronica'
    date: '21.08.2015'
    description: 'Slow beats, deep basses, beautiful, old synthesizers sounds, and on the top this beautiful, warm voice. Alternative meets electronic.'
  - id: 036
    title: 'Aurel den Bossa - Oups!'
    genre: 'Techno/Tech-house/Deep House'
    date: '17.07.2015'
    description: '„Oups!“ is modern, is now, is unique. Six tracks for the dance floor. Deep house, tech-house and techno, a multi-faceted, beautiful album and each track has his uniqueness.'
  - id: 035
    title: 'Niki Plisic - Night Story'
    genre: 'Deep House / Deep Techno'
    date: '12.06.2015'
    description: 'With his EP „Night Story“ he packages us a wide range of electronic music. Five techno and house tracks with sparkling sounds, deep basses and crispy beats.'
  - id: 034
    title: 'K-N-I-G-H-T - Moon'
    genre: 'Techno'
    date: '24.04.2015'
    description: 'In the wood at night, driving on a fast boat, skydiving, adrenaline, a thunderstorm and sometimes some sunbeams shaft through the black clouds.'
  - id: 033
    title: 'Digitule - 24'
    genre: 'Deep Techno / Deep House'
    date: '10.04.2015'
    description: 'Deep, melancholic, flash lights, beautiful melodies, thrilling soundscapes and fresh arrangements.'
  - id: 032
    title: 'Projekt Gestalten - Life'
    genre: 'Techno'
    date: '27.03.2015'
    description: 'A small, dark, industrial room, sweat dripping down from the ceiling, flashing lights, mist in the air, ecstasis, moving bodies, smiling people.'
  - id: 031
    title: 'Aurel den Bossa - First time'
    genre: 'House/Deep House/Techno'
    date: '13.03.2015'
    description: '„First time“ is, like the name says, the first EP of Aurel den Bossa. A multifaceted, beautiful EP, dreamy, touching, something for the heart, with melodies you want to listen to them again and again.'
  - id: 030
    title: 'Various Artists - Best of Dirty Cat Records Vol. 2'
    genre: 'All kind of electronic genres'
    date: '27.02.2015'
    description: 'Another great compilation, of hight talented artists, another mixture of high quality electronic music that moves, the second best of Dirty Cat Records.'
  - id: 029
    title: 'Hansgod - Pulp'
    genre: 'Techno/House/Deep House'
    date: '14.02.2015'
    description: 'With his new EP „Pulp“, Hansgod shows us a wide range of electronic music: a deep house track with some nice vocals, a beautiful house track and a new version of “Nogroup”.'
  - id: 028
    title: 'Hobo Baggins - Relikte historischer Nutzungen'
    genre: 'Techno/Deep House'
    date: '30.01.2015'
    description: 'Twelve beautiful, grooving, well conceived, cheeky, deep house, techno tracks, produced with a lot of feelings and sensibility.'
  - id: 027
    title: 'Twilight Kamikaze - Serpent Songs of Love'
    genre: 'IDM'
    date: '16.01.2015'
    description: 'Serpent Songs of love is a journey through different electronic music genres, a mixture of techno, electronica, industrial, breakbeat, dnb, experimental and rock with powerful vocals. Driving beats and glitchy, creaky, analogue sounds.'
  - id: 026
    title: 'Twilight Kamikaze - Flashback'
    genre: 'IDM'
    date: '09.01.2015'
    description: 'A mixture of techno, breakbeat, rock and electro with powerful vocals. Artificial, beautiful, groovy, including some great remixes.'
  - id: 025
    title: 'Beat Frei - Schleudertrauma'
    genre: 'Techno'
    date: '03.10.2014'
    description: 'Five warm and dark techno tracks at its best. Analogue, dark soundscapes, mistery melodies, creaking beats, somewhere in the wood, in the jungle, heat and sweat and dancing.'
  - id: 024
    title: 'Antimaterium - Essence of awakening'
    genre: 'Techno'
    date: '19.09.2014'
    description: 'Five dark techno tracks for the dance floor. Mysterious soundscapes, crispy percussions, powerful beats.'
  - id: 023
    title: 'CUSCINO - Set Fire'
    genre: 'House/Techhouse'
    date: '27.07.2014'
    description: 'Set fire is a progressive house track, you can’t classify immediately, which makes it unique. Creaky analogue sounds and sophisticated melodies.'
  - id: 022
    title: 'Dani Corbalan - Tear it down'
    genre: 'House/Chillout'
    date: '06.07.2014'
    description: 'A journey through the wild west, a wonderful warm up track and two beautiful chill-out tracks for the sunset.'
  - id: 021
    title: 'Dirty Cat - Lights'
    genre: 'Techno/House/Deep House'
    date: '21.06.2014'
    description: 'A mixture of techno, house, rock and pop. Driving beats, crispy guitar sounds, playful percussions, warm melodies and Dirty Cats unique voice.'
  - id: 020
    title: 'Dirty Cat - Afterhour'
    genre: 'Techno/House/Deep House'
    date: '14.06.2014'
    description: '“Afterhour” is a beautiful chillout track for the morning, for sunny days, just right for the upcoming summer season. A smooth house track with warm guitar and synthesizer sounds and Dirty Cats unique voice.'
  - id: 019
    title: 'Hansgod - Afroots'
    genre: 'Techno/House/Minimal'
    date: '18.05.2014'
    description: 'Hansgod creates his own little world of soundscapes, crispy percussions, driving beats, dark, sensual, with warm, powerful sequencer sounds.'
  - id: 018
    title: 'Various Artists - Best of DCR Vol. 1'
    genre: 'All kind of electronic genres'
    date: '06.04.2014'
    description: 'A multifaceted mixture of the last two years: nine highly talented electronic artists at its best. Including exclusive track “Buttheads - Zeitmaschine”.'
  - id: 017
    title: 'Beat Frei - 2014 [Wut]'
    genre: 'Techno/Chillout/Soundtrack'
    date: '23.03.2014'
    description: 'He knows how to create sounds with analogue machines, how to compose thrilling songs, he is a master of all kind of genres. From classical music to deep techno songs, everyone will be satisfien'
  - id: 016
    title: 'Hansgod - Ice Cream'
    genre: 'Techno/Techhouse/Minimal'
    date: '08.12.2013'
    description: 'Hansgod creates mystery spaces, he tells fascinating stories and takes you along to his own little world of sounds.'
  - id: 015
    title: 'Dani Corbalan - Back from the depths'
    genre: 'House/Electro-House/Chillout'
    date: '01.12.2013'
    description: 'Dani Corbalan mixes skilful Electro with House and somewhere in melodies you can hear his Trance past.'
  - id: 014
    title: 'Angelo Fonfara - Menschen und Maschinen'
    genre: 'Breakbeat/Brokenbeat/Dub'
    date: '11.08.2013'
    description: 'Angelo Fonfara combines masterful beats and percussions with pumpy basses, let flash here and there some guitar- and synthesizer sounds and fizzy funny and yet still profound texts.'
  - id: 013
    title: 'Angelo Fonfara - Radiostation'
    genre: 'Breakbeat/Techno/House'
    date: '04.08.2013'
    description: 'A mixture of breakbeat and dub, witty and fresh, followed by a drum n’ bass remix made by himself and remixes by Dirty Cat and Kabelmann.'
  - id: 012
    title: 'Josh Tree - Balls'
    genre: 'Techno/Minimal'
    date: '22.05.2013'
    description: 'Driving beats, creaking basses, playfull percussions, unusual, trippy, home-made samples. When you don’t expect it, a bassdrum, a hihat, a snare beginns.'
  - id: 011
    title: 'Adis Gile - The city of mine'
    genre: 'Techno/Techhouse/House'
    date: '22.10.2012'
    description: 'In „the city of mine“ Adis Gile mixes crispy, playfull technobeats with funky synthesizer sounds and saxophone melodies.'
  - id: 010
    title: 'Buttheads - Stéréophonie Remixes'
    genre: 'Electro/House/Disco/Techno'
    date: '30.07.2012'
    description: 'A little EP with two remixes made by SWEAR and Buttheads themselves as a free download.'
  - id: 009
    title: 'Buttheads - Stéréophonie'
    genre: 'Electro/House/Disco'
    date: '30.07.2012'
    description: 'What you get is funky melodies, distorted synthesizers and crispy, cheecky beats, a mixture of funk, electro and disco-house.'
  - id: 008
    title: 'Tammo - Plugged'
    genre: 'Alternative/Breakbeat/Techno'
    date: '16.07.2012'
    description: 'Beautifull, dreamy melodies, suddenly distorted guitar riffs, playfull breakbeats and slowly, melancholy ballads too.'
  - id: 007
    title: 'Tammo - Zukunftsmode'
    genre: 'Breakbeat/House/Ambient'
    date: '09.07.2012'
    description: 'Zukunftsmode is playfull, dreamy, a thrilling story, warm sunbeams in the morning, or just a journey down to the south.'
  - id: 006
    title: 'Dirty Cat - Inside'
    genre: 'Techno/House'
    date: '26.11.2011'
    description: 'Dirty Cat mixes creaking techno beats with droning basses and trancy house melodies. Eight danceable tech-house tracks for the dancefloor.'
  - id: 005
    title: 'Kabelmann - Schaltraum'
    genre: 'Techno/Electronic Industrial'
    date: '31.10.2011'
    description: 'The name of the album promises exactly what you expect of it: analogue, dirty sounds of modularsynthesizers. And he knows how to handle with analog modularsynthesizers.'
  - id: 004
    title: 'Dirty Cat - Autumn'
    genre: 'Techno/Deep House'
    date: '05.09.2011'
    description: 'His driving beats are playfull and his melodies invites you to dream and float away, for everyone who isn’t satisfied by this summer and who wants to embellish the autumn a little bit.'
  - id: 003
    title: 'Tammo - Bare Prison'
    genre: 'Electronica/House/Dance'
    date: '29.08.2011'
    description: 'Tammo succeeds in creating a very special atmosphere and takes you on a journey to somewhere between soundtrack, club music and chill-out.'
  - id: 002
    title: 'Dirty Cat - Springtime'
    genre: 'Techno/House'
    date: '11.02.2011'
    description: 'Warm melodies, driving, playful beats, crispy percussions, deep bass lines. Including “Happiness”, a good feel track for the after-hours.'
  - id: 001
    title: 'Dirty Cat - 012010'
    genre: 'Techno/Minimal'
    date: '01.01.2011'
    description: 'Eleven minimal, house dancefloor tracks with a lot of percussion, heavy basslines and dreamy melodies.'
---
