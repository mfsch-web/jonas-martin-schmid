---
title: DATES
---

<p><p><strong>Releases:</strong></p>
<p>11.09.2015: DCR039 Adis G - Bromazepam</p>

<p> </p>
<p> </p>
<p> </p>

<p><strong>Live:</strong> </p>
<p><a href="http://www.3000-festival.de" target="_blank">16.08.2015: Jan Plötzlich @ 3000 Grad Festival </a></p>
<p><a href="https://www.facebook.com/events/850618921693831/" target="_blank">05.09.2015: Hormonfabrik presents Das Land der fünf Sinne #2 with Jan Plötzlich live @ Loftus, Berlin</a></p></p>
