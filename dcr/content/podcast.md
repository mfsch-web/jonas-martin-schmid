---
title: PODCAST
---

<div class="soundcloud-wrapper">
  <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/204978820&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
  <p>DCRPODCAST005 Aurel Den Bossa - Playa den Bossa Time by Dirty Cat Records</p>
</div>

<div class="soundcloud-wrapper">
  <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/198927173&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
  <p>DCRPODCAST004 CUSCINO - Future Bass (Session 00 Live) by Dirty Cat Records</p>
</div>

<div class="soundcloud-wrapper">
  <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/157666221&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
  <p>DCRPODCAST003 Dirty Cat - Summer Feelings (live) by Dirty Cat Records</p>
</div>

<div class="soundcloud-wrapper">
  <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/147548845&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
  <p>DCRPODCAST002 CUSCINO - I Will Find You In The Dark by Dirty Cat Records</p>
</div>

<div class="soundcloud-wrapper">
  <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/129654695&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
  <p>DCRPODCAST001 Adis Gile - Those Feelings by Dirty Cat Records</p>
</div>
