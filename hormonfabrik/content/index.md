---
title: NEWS
---

<p>

<h2>Das Land der fünf Sinne #3</h2>

<p><strong>12.12.2015 @ Loftus Hall, Maybachufer 48, 12045 Berlin</strong></p>

<p><span>Wir freuen uns, unsere dritte"Das Land der fünf Sinne" Party zu präsentieren. Es wird bunter, schöner und besser. A<span>uf die Ohren ein ausgewähltes Line-Up elektronischer Tanzmusik, zur optischen Reizüberflutung trägt ein Team von ausgefallenen Visualisten bei, auf die Zunge gibt es flüssiges Elixier und in regelmässigen Abständen frische Früchte, für die olfaktorische Wahrnehmung sorgt unsere geheimnisvolle Dufttankstelle und für die Verminderung von Dehydrierung kann man sich an Erfrischungsduschen ergötzen. Ein Spielplatz für Erwachsene, eineTanzwiese für Tanzwütige. </span></span></p>

<p>
<span>LINE UP:</span>
<br />
<span>\*\*\*</span>
<br />
<span>MONOLINK DJset (3000°, Acker Records)</span>
<br /><br />
<span>Jan Plötzlich live (Hormonfabrik, 3000°, Dirty Cat Records)</span>
<br /><br />
<span>Pablo Denegri live (Dumb Unit,</span><span class="text_exposed_show"> Aula Magna, Kumquat, Little Helpers)
<br />
<br />Søren Linke (XLR1507)
<br />
<br />Angelo Fonfara live (Hormonfabrik. Dirty Cat Records)
<br />
<br />Almada live (Hormonfabrik)
<br />
<br />\*\*\*
<br />Artist-Links:
<br /><a href="https://soundcloud.com/monolink" rel="nofollow nofollow" target="_blank">https://soundcloud.com/monolink</a>
<br />
<a href="https://www.facebook.com/l.php?u=https%3A%2F%2Fsoundcloud.com%2Fpablo-denegri&amp;h=HAQESzkJX&amp;enc=AZPnWM_Zmho6XfKPI89xRZp7NCRDq4PK_Tkz6ObM88EWyUY-TMVcmhfdAHQr46PqX1Q&amp;s=1" rel="nofollow nofollow" target="_blank">https://soundcloud.com/pablo-denegri</a>
<br />
<a href="http://l.facebook.com/l.php?u=http%3A%2F%2Fsoundcloud.com%2Fjanploetzlich&amp;h=4AQFgfvxA&amp;enc=AZMq97XINxJ9bQdQ-tO4qUIaH5_bkzmBt4vLmmF7V8XBokwhVJOIdOdV3wXa_jZ3xxE&amp;s=1" rel="nofollow nofollow" target="_blank">http://soundcloud.com/janploetzlich</a>
<br />
<a href="https://www.facebook.com/Soundchaot" rel="nofollow">https://www.facebook.com/Soundchaot</a>
<br />
<a href="http://l.facebook.com/l.php?u=http%3A%2F%2Fsoundcloud.com%2Falmada-foc&amp;h=WAQGnyPmF&amp;enc=AZNeTdG14URYoZ0ejOBRvBkmMf4_tVotiljxACaMDIwucyyBaxnM-8dRWkLMeTtWdv4&amp;s=1" rel="nofollow nofollow" target="_blank">http://soundcloud.com/almada-foc</a>
<br />
<a href="https://www.facebook.com/l.php?u=https%3A%2F%2Fsoundcloud.com%2Ffonfara&amp;h=uAQGZqvLi&amp;enc=AZPT8ffGpc5fQwPhvnhN91YbxVzqii310k1DsQHQaqcYkwDSU7Qgf6KXhCVJRqH1iBU&amp;s=1" rel="nofollow nofollow" target="_blank">https://soundcloud.com/fonfara</a>
<br />
<br />Links:
<br />
<a href="http://www.facebook.com/hormonfabrik" rel="nofollow">http://www.facebook.com/hormonfabrik</a>
<br />
<a href="http://www.facebook.com/dirtycatrecords" rel="nofollow">http://www.facebook.com/dirtycatrecords</a>
<br />
<a href="http://www.facebook.com/loftus.hall" rel="nofollow">http://www.facebook.com/loftus.hall</a>
<br />\*\*\*</span>
</p>

</p>

<p><div id="HTMLBlock62" class="HTMLBlock"><script src="http://connect.facebook.net/de_DE/all.js#xfbml=1"></script><fb:like-box href="http://www.facebook.com/hormonfabrik" width="292" height="200" show_faces="true" stream="false" header="false"></fb:like-box></div></p>
