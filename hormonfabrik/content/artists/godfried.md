---
title: Godfried
---

<div id="ccm-layout-wrapper-40" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-3-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-3-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:46%">	<div id="blockStyle120HauptbereichLayout1Cell14" class=" ccm-block-styles" >
<img border="0" class="ccm-image-block" alt="" src="../img/Godfried-big.jpg" width="400" height="400" /></div></div><div class="ccm-layout-3-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:53.99%"><h2 style="text-align: left;">Godfried</h2>
<p style="text-align: left;"><em>experimental electronic music</em></p>
<p style="text-align: left;"><em><a href="http://soundcloud.com/godfried-godfried" target="_blank"><img src="../img/soundcloud2.jpg" alt="soundcloud2.jpg" width="20" height="20" /></a> <a href="https://www.facebook.com/pages/GodFried/840593359313122?ref=hl" target="_blank"><img src="../img/facebook2.jpg" alt="facebook2.jpg" width="20" height="20" /></a></em></p>
<p style="text-align: left;">GodFried is actor and musician, stated in Berlin/Germany. With his sound he tries to create a bridge between his weird singing-songwriting and electronic tunes, not attaching to any genres, but always creating something that just sounds like... GodFried.</p>
<p style="text-align: left;"> </p>
<p style="text-align: left;"><strong>Booking</strong>: booking[at]hormonfabrik.de</p></div><div class="ccm-spacer"></div></div></div></div><p>	<div id="blockStyle111Hauptbereich5" class=" ccm-block-styles" >

<div id="HTMLBlock111" class="HTMLBlock">
<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/230664344&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe></div></div></p>
