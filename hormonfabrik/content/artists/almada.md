---
title: Almada
---

<div id="ccm-layout-wrapper-39" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-1-2" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-2 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-1-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:46%">	<div id="blockStyle118HauptbereichLayout2Cell12" class=" ccm-block-styles" >
<img border="0" class="ccm-image-block" alt="" src="../img/Almada-big.jpg" width="400" height="415" /></div></div><div class="ccm-layout-1-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:53.99%"><h2 style="text-align: left;">Almada</h2>
<p style="text-align: left;"><em>techno</em></p>
<p style="text-align: left;"><em><a href="http://soundcloud.com/almada-foc" target="_blank"><img src="../img/soundcloud2.jpg" alt="soundcloud2.jpg" width="20" height="20" /></a> <a href="https://www.facebook.com/tangocrashofficial?fref=ts" target="_blank"><img src="../img/facebook2.jpg" alt="facebook2.jpg" width="20" height="20" /></a></em></p>
<p style="text-align: left;">Daniel Almada is an Argentinian musician, who works since 1988 as independent composer, Sound Designer and Performer. He wrote the soundtrack and realize the sound design of „Die Frau mit den 5 Elefanten“ from Vadim Jendreyko/- Quartz 2010 – Besten Schweizer Dokumentarfilm , „4 de Julio, la masacre de San Patricio“ Argentinische Kino Dok Film 2 Cóndor de Plata (besten Dokumentarfilm 2008 y besten Dok-Drehbuch 2008) „Signis Preis 2008“ für Lateinamerika Garten der Klänge“ Nicola Belluci/Erste Preis am 45. Solothurner Filmtage «Prix de Soleure» , u.a. He founded in 2003 together with Martin Iannaccone the group “Tango Crash” which received in 2004 the award “ Ruth Deutsche Weltmusikpreis : Newcomer „ And in 2008 for the production „Bailá Querida“ the „Preis der Deutsche Schallplattenkritik“. They performed around 200 concerts worldwide.</p>
<p style="text-align: left;"> </p>
<p style="text-align: left;"><strong>Booking</strong>: booking[at]hormonfabrik.de</p></div><div class="ccm-spacer"></div></div></div></div><div id="ccm-layout-wrapper-38" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-2-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-2-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:100%">	<div id="blockStyle96HauptbereichLayout1Cell110" class=" ccm-block-styles" >

<div id="HTMLBlock96" class="HTMLBlock">
<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/201220015&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe></div></div>	<div id="blockStyle97HauptbereichLayout1Cell19" class=" ccm-block-styles" >

<div id="HTMLBlock97" class="HTMLBlock">
<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/208244314&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe></div></div></div><div class="ccm-spacer"></div></div></div></div>
