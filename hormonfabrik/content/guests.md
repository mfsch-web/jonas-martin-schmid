---
title: GUESTS
---

<p>
  <p><a href="http://soundcloud.com/anders-hellberg" target="_blank">Anders Hellberg <br />(Sontagmorgen/Arts)</a></p>
  <p><a href="https://soundcloud.com/fonfara" target="_blank">Angelo Fonfara</a><br /><a href="https://soundcloud.com/fonfara" target="_blank">(Dirty Cat Records)</a></p>
  <p><a href="http://soundcloud.com/dj-johannes-1" target="_blank">Johannes Holmen <br />(Sonntagmorgen/Arts)</a></p>
  <p><a href="http://soundcloud.com/lambentlamb" target="_blank">Lamb Ent <br />(dtape)</a></p>
  <p><a href="http://www.soundcloud.com/monolink" target="_blank">Monolink</a><br /><a href="http://www.soundcloud.com/monolink" target="_blank">(3000°, Acker Records)</a></p>
  <p><a href="https://soundcloud.com/pablo-denegri" target="_blank">Pablo Denegri <br />(Dumb Unit, Aula Magna, Kumquat, Little Helpers, Buenos Aires)</a><a href="http://soundcloud.com/anders-hellberg" target="_blank"><br /></a></p>
  <p><a href="https://soundcloud.com/soundchaot" target="_blank">Søren Linke <br />(<span>XLR1507)</span></a><a href="http://soundcloud.com/dj-johannes-1" target="_blank"><br /></a></p>
  <p><a href="http://vimeo.com/tekhneaudiovisual" target="_blank">TECKHNE <br />(Live Visuals)</a></p>
  <p><a href="https://soundcloud.com/tangocrash" target="_blank">Tango Crash <br />(Pocket Trio)</a></p>
  <p><a href="https://soundcloud.com/pablo-denegri" target="_blank"><span> </span></a></p>
  <p> </p>
  <p> </p>
  <p> </p>
</p>
