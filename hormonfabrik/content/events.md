---
title: EVENTS
---

<p>
<p class="ueberschr">Das Land der fünf Sinne</p>
<p><span>Auf die Ohren gibt es ein ausgewähltes Line-Up elektronischer Tanzmusik, zur optischen Reizüberflutung trägt ein Team von ausgefallenen Visualisten bei, auf die Zunge gibt es flüssiges Elixier und in regelmässigen Abständen frische Früchte, für die olfaktorische Wahrnehmung sorgt unsere geheimnisvolle Dufttankstelle und für die Verminderung von Dehydrierung kann man sich an Erfrischungsduschen ergötzen. Ein Spielplatz für Erwachsene, eine</span><span class="text_exposed_show">Tanzwiese für Tanzwütige.</span></p>
</p>
