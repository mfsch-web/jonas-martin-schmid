---
title: ARTISTS
---

<p><table border="0" cellspacing="10"><tbody>

<tr>
  <td align="left" valign="top"><img src="img/Almada.jpg" alt="Almada.jpg" width="200" height="208" /></td>
  <td align="left" valign="top">
    <p class="ueberschr">ALMADA</p>
    <p><a href="http://soundcloud.com/almada-foc" target="_blank"><img src="img/soundcloud2.jpg" alt="soundcloud2.jpg" width="20" height="20" /></a> <a href="https://www.facebook.com/tangocrashofficial?fref=ts" target="_blank"><img src="img/facebook2.jpg" alt="facebook2.jpg" width="20" height="20" /></a></p>
    <p><span>Daniel Almada is an Argentinian musician, who works since 1988 as independent composer, Sound Designer and Performer. He wrote the soundtrack and realize the sound design of „Die Frau mit den 5 Elefanten“ from Vadim Jendreyko/- Quartz 2010 – Besten Schweizer Dokumentarfilm , „4 de Julio, la masacre de San Patricio“ Argentinische Kino Dok Film 2 Cóndor de Plata (besten Dokumentarfilm 2008 y besten Dok-Drehbuch 2008) „Signis Preis 2008“ für Lateinamerika Garten der Klänge“ Nicola Belluci/Erste Preis am 45. Solothurner Filmtage «Prix de Soleure» , u.a. He founded in 2003 together with Martin Iannaccone the group “Tango Crash” which received in 2004 the award “ Ruth Deutsche Weltmusikpreis : Newcomer „ And in 2008 for the production „Bailá Querida“ the „Preis der Deutsche Schallplattenkritik“. They performed around 200 concerts worldwide.</span> <br /><a title="Almada" href="artists/almada">&gt; more</a></p>
  </td>
</tr>

<tr><td align="left" valign="top"> </td><td align="left" valign="top"> </td></tr>

<tr>
  <td align="left" valign="top"><img src="img/godfried2.jpg" alt="godfried2.jpg" width="200" height="200" /></td>
  <td align="left" valign="top">
    <p class="ueberschr">GODFRIED</p>
    <p><a href="http://soundcloud.com/godfried-godfried" target="_blank"><img src="img/soundcloud2.jpg" alt="soundcloud2.jpg" width="20" height="20" /></a> <a href="https://www.facebook.com/pages/GodFried/840593359313122?ref=hl" target="_blank"><img src="img/facebook2.jpg" alt="facebook2.jpg" width="20" height="20" /></a></p>
    <p><span>GodFried is actor and musician, stated in Berlin/Germany. With his sound he tries to create a bridge between his weird singing-songwriting and electronic tunes, not attaching to any genres, but always creating something that just sounds like... Godfried<br /><a title="Godfried" href="artists/godfried">&gt; more</a></span></p>
    <p><span> </span></p>
    <p><span> </span></p>
  </td>
</tr>

<tr><td align="left" valign="top"> </td><td align="left" valign="top"> </td></tr>

<tr>
  <td align="left" valign="top"><img src="img/janploetzlich_200.jpg" alt="janploetzlich_200.jpg" width="200" height="200" /></td>
  <td align="left" valign="top">
    <p class="ueberschr">JAN PLOETZLICH</p>
    <p><a href="http://soundcloud.com/janploetzlich" target="_blank"><img src="img/soundcloud2.jpg" alt="soundcloud2.jpg" width="20" height="20" /></a> <a href="http://www.facebook.com/janploetzlich" target="_blank"><img src="img/facebook2.jpg" alt="facebook2.jpg" width="20" height="20" /></a> <a href="http://www.residentadvisor.net/dj/janplotzlich" target="_blank"><img src="img/residentadvisor2.jpg" alt="residentadvisor2.jpg" width="20" height="20" /></a></p>
    <p><span class="text_exposed_show"><span>Jonas Martin Schmid aka Jan Plötzlich is producer, live-act, actor, musician and owner of Dirty Cat Records. He produces melodic downtempo/deep house. He combines deep beats and basses with light, beautiful melodies, guitar and trumpet sounds and vocals. His musical career began with trumpet, jazz, a guitar, a piano, a harmonica, a djembe and sentimental love-songs. This was followed by three years as a singer and guitarist in a new-wave / rockband. In search of new sounds, he began to combine his musical roots with electronic elements. It originated a mixture of techno, rock and house. </span> <br /><a title="Jan Plötzlich" href="artists/jan-ploetzlich">&gt; more</a></span> </p>
  </td>
</tr>

</tbody></table></p>
