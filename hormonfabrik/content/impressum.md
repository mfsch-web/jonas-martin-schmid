---
title: Impressum
---

<p><p>Hormonfabrik <br />Jonas Martin Schmid<br />Großbeerenstrasse 67<br />10963 Berlin</p>
<p><br />info[at]hormonfabrik.de | booking[at]hormonfabrik.de</p>
<p> </p>
<p><span class="textbold">Haftungshinweis</span><br /><br /><span>Die Mitteilung sämtlicher Informationen auf diesem Website erfolgt nach bestem Wissen und Gewissen, jedoch “ohne Gewähr” für die Richtigkeit und Vollständigkeit. Jegliche Haftung für Schäden, die durch Fehler auf diesem Website entstehen, ist, soweit gesetzlich zulässig, ausgeschlossen. Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich. </span></p></p>
