---
title: VITA
---

<div id="ccm-layout-wrapper-205" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-12-2" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-2 "><div class="ccm-layout-row ccm-layout-row-1">

<div class="ccm-layout-12-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:55%"><div id="blockStyle474HauptbereichLayout2Cell135" class=" ccm-block-styles">
<p><span style="text-decoration: underline;"><strong>FACTS</strong></span></p>
<table cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td width="120" valign="top" align="left"><strong>Beruf</strong></td>
<td width="*" valign="top" align="left">Schauspieler und Musiker</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Geboren</strong></td>
<td valign="top" align="left">1984 in Arlesheim/Schweiz</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Grösse</strong></td>
<td valign="top" align="left">181 cm</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Gewicht</strong></td>
<td valign="top" align="left">75 KG</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Haarfarbe</strong></td>
<td valign="top" align="left">dunkelbraun</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Augenfarbe</strong></td>
<td valign="top" align="left">blau-grün</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Statur</strong></td>
<td valign="top" align="left">athletisch, sportlich</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Nationalität</strong></td>
<td valign="top" align="left">Schweiz</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Sprachen</strong></td>
<td valign="top" align="left">Deutsch (Muttersprache), Englisch (fliessend), Französisch (gut)</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Dialekte</strong></td>
<td valign="top" align="left">Schweizerdeutsch (Basel / Bern)</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Stimme</strong></td>
<td valign="top" align="left">Bariton, Beatbox</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Instrumente</strong></td>
<td valign="top" align="left">Trompete, Klavier, Gitarre, Schlagzeug, Percussion, Mundharmonika</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Sonstiges</strong></td>
<td valign="top" align="left">Akrobatik, Jonglage, Seiltanz, Tanzen, Fechten, Tennis, Handball, Fussball, Capoeira</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Wohnort</strong></td>
<td valign="top" align="left">Berlin</td>
</tr>
<tr>
<td valign="top" align="left"><strong>Wohnmöglichkeit</strong></td>
<td valign="top" align="left">
<p>Berlin, München, Hamburg, Wien, Hannover, Zürich, Basel</p>
</td>
</tr>
</tbody>
</table></div></div>

<div class="ccm-layout-12-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:44.99%"><div id="blockStyle442HauptbereichLayout2Cell234" class=" ccm-block-styles">
<p style="text-align: left;">&nbsp;<img src="img/jonasmartinschmid-p2.jpg" alt="jonasmartinschmid-p2.jpg" width="400" height="267"></p>
<p style="text-align: left;">&nbsp;<a href="files/JonasMartinSchmidVita2015.pdf">DOWNLOAD&nbsp;VITA ALS PDF</a>&nbsp;&nbsp;</p></div></div>

<div class="ccm-spacer"></div>

</div></div></div>







<div id="ccm-layout-wrapper-207" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-14-4" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-4 "><div class="ccm-layout-row ccm-layout-row-1">

<div class="ccm-layout-14-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:55%"><div id="blockStyle475HauptbereichLayout4Cell142" class=" ccm-block-styles">
<p style="text-align: justify;"><span style="text-decoration: underline;"><strong>AUSBILDUNG</strong></span></p>
<table cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td width="70" valign="top" align="left">2006-2010</td>
<td width="*" valign="top" align="left">
<p>Otto Falckenberg Schule München<br>(Camera Acting bei Johannes 
Fabrik, Clownsworkshop bei Paco Gonzalez,&nbsp;Hörspiel- und 
Synchronisationsworkshop bei Frauke Poolmann)</p>
</td>
</tr>
<tr>
<td valign="top" align="left">2015</td>
<td valign="top" align="left">Schauspiel Workshop über die 6 Grundemotionen bei Bruno Cathomas</td>
</tr>
</tbody>
</table></div></div>

<div class="ccm-layout-14-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:44.99%"><div id="blockStyle476HauptbereichLayout4Cell236" class=" ccm-block-styles">
<p style="text-align: justify;"><span style="text-decoration: underline;"><strong>AUSZEICHNUNGEN/PREISE</strong></span></p>
<table cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td width="70" valign="top" align="left">2010</td>
<td width="*" valign="top" align="left">Junge Talente Schweiz</td>
</tr>
<tr>
<td valign="top" align="left">2008 &amp;<br>2009</td>
<td valign="top" align="left">Studienpreis Schauspiel des Migros-Kulturprozents und der Ernst Göhner Stiftung&nbsp;</td>
</tr>
<tr>
<td valign="top" align="left">2005</td>
<td valign="top" align="left">Kurt Hübner Preis &amp; Pokal des Impulse Festivals für „Fucking Åmål“ unter der Regie von Sebastian Nübling.</td>
</tr>
</tbody>
</table></div></div>

<div class="ccm-spacer"></div>

</div></div></div>





<div id="ccm-layout-wrapper-206" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-15-3" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-3 "><div class="ccm-layout-row ccm-layout-row-1">

<div class="ccm-layout-15-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:100%"><p><span style="text-decoration: underline;"><strong>FILM</strong></span></p>
<table style="width: 100%;" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td style="width: 50px;" valign="top" align="left"><strong>Jahr</strong></td>
<td style="width: 250px;" valign="top" align="left"><strong>Titel</strong></td>
<td style="width: 200px;" valign="top" align="left"><strong>Rolle</strong></td>
<td style="width: 200px;" valign="top" align="left"><strong>Regie</strong></td>
<td valign="top" align="left"><strong>Produktion</strong></td>
</tr>
<tr>
<td valign="top" align="left">2014</td>
<td valign="top" align="left">Sturm im Paradies</td>
<td valign="top" align="left">Edward (HR)</td>
<td valign="top" align="left">Pia Elser, Corinne Santucci</td>
<td valign="top" align="left">Macromedia HS Berlin</td>
</tr>
<tr>
<td valign="top" align="left">2014</td>
<td valign="top" align="left">Teaser für Film "Inside me!"</td>
<td valign="top" align="left">Infizierter (HR)</td>
<td valign="top" align="left">Daniel Göttler</td>
<td valign="top" align="left">MirrorGate Pictures</td>
</tr>
<tr>
<td valign="top" align="left">2014</td>
<td valign="top" align="left">LBS Werbespot</td>
<td valign="top" align="left">Herr Hoffmann (HR)</td>
<td valign="top" align="left">Florian Daferner</td>
<td valign="top" align="left">DAF FILM</td>
</tr>
<tr>
<td valign="top" align="left">2013</td>
<td valign="top" align="left">Breath</td>
<td valign="top" align="left">Marc (HR)</td>
<td valign="top" align="left">Phil Emonds</td>
<td valign="top" align="left">Kurzfilm</td>
</tr>
<tr>
<td valign="top" align="left">2012</td>
<td valign="top" align="left">Der Massendefekt des Todes</td>
<td valign="top" align="left">Martin (HR)</td>
<td valign="top" align="left">Dobromir Bilca</td>
<td valign="top" align="left">Kurzfilm</td>
</tr>
<tr>
<td valign="top" align="left">2011</td>
<td valign="top" align="left">Jackpot</td>
<td valign="top" align="left">Mario (NR)</td>
<td valign="top" align="left">Dennis Scherr</td>
<td valign="top" align="left">Filmakademie Ludwigsburg</td>
</tr>
<tr>
<td valign="top" align="left">2011</td>
<td valign="top" align="left">Begegnen</td>
<td valign="top" align="left">Peter (HR)</td>
<td valign="top" align="left">Jörg-Michael Schneider</td>
<td valign="top" align="left">Merz Akademie Stuttgart</td>
</tr>
<tr>
<td valign="top" align="left">2011</td>
<td valign="top" align="left">Marie (AT)</td>
<td valign="top" align="left">Thomas (NR)</td>
<td valign="top" align="left">Nicolai Zeitler</td>
<td valign="top" align="left">HFG Karlsruhe</td>
</tr>
<tr>
<td valign="top" align="left">2011</td>
<td valign="top" align="left">Glas/Glass</td>
<td valign="top" align="left">Julian (NR)</td>
<td valign="top" align="left">Eva Lechner</td>
<td valign="top" align="left">Plan B Films</td>
</tr>
<tr>
<td valign="top" align="left">2010</td>
<td valign="top" align="left">Offen</td>
<td valign="top" align="left">Marco (HR)</td>
<td valign="top" align="left">Marcel Gisler</td>
<td valign="top" align="left">Junge Talente Schweiz</td>
</tr>
<tr>
<td valign="top" align="left">2010</td>
<td valign="top" align="left">McDonalds Werbespot</td>
<td valign="top" align="left">Moka Rocka (HR)</td>
<td valign="top" align="left">Christian Ditter</td>
<td valign="top" align="left">epcommercials</td>
</tr>
<tr>
<td valign="top" align="left">2009</td>
<td valign="top" align="left">Für immer 30</td>
<td valign="top" align="left">Werber (NR)</td>
<td valign="top" align="left">Andi Niessner</td>
<td valign="top" align="left">ARD</td>
</tr>
<tr>
<td valign="top" align="left">2005</td>
<td valign="top" align="left">Blind Date</td>
<td valign="top" align="left">Er (HR)</td>
<td valign="top" align="left">Hannes Rüttimann</td>
<td valign="top" align="left">Kurzfilm ZHDK</td>
</tr>
<tr>
<td valign="top" align="left">2003</td>
<td valign="top" align="left">The Contest</td>
<td valign="top" align="left">Bela (HR)</td>
<td valign="top" align="left">Matt Inauen</td>
<td valign="top" align="left">FHBB</td>
</tr>
</tbody>
</table>
<p><br><br><span style="text-decoration: underline;"><strong>THEATER (Auswahl)</strong></span></p>
<table style="width: 100%;" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td width="50px" valign="top" align="left"><strong>Jahr</strong></td>
<td style="width: 250px;" valign="top" align="left"><strong>Titel</strong></td>
<td width="200px" valign="top" align="left"><strong>Rolle</strong></td>
<td style="width: 200px;" valign="top" align="left"><strong>Regie</strong></td>
<td valign="top" align="left"><strong>Produktion</strong></td>
</tr>
<tr>
<td valign="top" align="left">2016</td>
<td valign="top" align="left">Troilus und Cressida</td>
<td valign="top" align="left">Musiker</td>
<td valign="top" align="left">Rafael Sanchez</td>
<td valign="top" align="left">Schauspiel Köln</td>
</tr>
<tr>
<td valign="top" align="left">2015</td>
<td valign="top" align="left">Tödliche Schönheit</td>
<td valign="top" align="left">Olaf, Malovsky</td>
<td valign="top" align="left">Timo Klein</td>
<td valign="top" align="left">Papilliotheater Berlin</td>
</tr>
<tr>
<td valign="top" align="left">2015</td>
<td valign="top" align="left">Grimmige Walkacts</td>
<td valign="top" align="left">Hänsel, Prinz, Frosch, u.a.</td>
<td valign="top" align="left">Timo Klein</td>
<td valign="top" align="left">Papilliotheater Berlin</td>
</tr>
<tr>
<td valign="top" align="left">2010/15</td>
<td valign="top" align="left">Dylan - The Times They Are&nbsp;<br>A-Changin</td>
<td valign="top" align="left">Ensemble, Musiker</td>
<td valign="top" align="left">Heiner Kondschak</td>
<td valign="top" align="left">Württembergische Landesbühne Esslingen</td>
</tr>
<tr>
<td valign="top" align="left">2014</td>
<td valign="top" align="left">Himmel und Hölle! - Dazwischen die Leut!</td>
<td valign="top" align="left">Saz</td>
<td valign="top" align="left">Matthias Brenner</td>
<td valign="top" align="left">Württembergische Landesbühne Esslingen</td>
</tr>
<tr>
<td valign="top" align="left">2014</td>
<td valign="top" align="left">Mann ist Mann</td>
<td valign="top" align="left">Jeraiah Jip, Musiker</td>
<td valign="top" align="left">Manuel Soubeyrand</td>
<td valign="top" align="left">Württembergische Landesbühne Esslingen</td>
</tr>
<tr>
<td valign="top" align="left">2013</td>
<td valign="top" align="left">Kabale und Liebe</td>
<td valign="top" align="left">Ferdinand</td>
<td valign="top" align="left">Alejandro Quintana</td>
<td valign="top" align="left">Württembergische Landesbühne Esslingen</td>
</tr>
<tr>
<td valign="top" align="left">2012</td>
<td valign="top" align="left">Viel Lärm um Nichts</td>
<td valign="top" align="left">Claudio</td>
<td valign="top" align="left">Tilo Esche</td>
<td valign="top" align="left">Württembergische Landesbühne Esslingen</td>
</tr>
<tr>
<td valign="top" align="left">2012</td>
<td valign="top" align="left">Die Verschwörung des Fiesco zu Genua</td>
<td valign="top" align="left">Burgognino</td>
<td valign="top" align="left">Sewan Latchinian</td>
<td valign="top" align="left">Württembergische Landesbühne Esslingen</td>
</tr>
<tr>
<td valign="top" align="left">2011</td>
<td valign="top" align="left">Hexenjagd</td>
<td valign="top" align="left">Danforth</td>
<td valign="top" align="left">Sandrine Hutinet</td>
<td valign="top" align="left">Württembergische Landesbühne Esslingen</td>
</tr>
<tr>
<td valign="top" align="left">2010</td>
<td valign="top" align="left">John Gabriel Borkman</td>
<td valign="top" align="left">Erhardt</td>
<td valign="top" align="left">Alejandro Quintana</td>
<td valign="top" align="left">Württembergische Landesbühne Esslingen</td>
</tr>
<tr>
<td valign="top" align="left">2009</td>
<td valign="top" align="left">Katzelmacher</td>
<td valign="top" align="left">Jorgos</td>
<td valign="top" align="left">Aus Greidanus jr.</td>
<td valign="top" align="left">Münchner Kammerspiele</td>
</tr>
<tr>
<td valign="top" align="left">2008</td>
<td valign="top" align="left">Richard III</td>
<td valign="top" align="left">Edward/Richard</td>
<td valign="top" align="left">Claudia Bauer</td>
<td valign="top" align="left">Münchner Kammerspiele</td>
</tr>
<tr>
<td valign="top" align="left">2007</td>
<td valign="top" align="left">Mamma Medea</td>
<td valign="top" align="left">Frontis</td>
<td valign="top" align="left">Stephan Kimmig</td>
<td valign="top" align="left">Münchner Kammerspiele</td>
</tr>
<tr>
<td valign="top" align="left">2006</td>
<td valign="top" align="left">Freie Sicht aufs Mittelmeer</td>
<td valign="top" align="left">Sohn</td>
<td valign="top" align="left">Dani Levy</td>
<td valign="top" align="left">Theater Basel</td>
</tr>
<tr>
<td valign="top" align="left">2006</td>
<td valign="top" align="left">Rückkehr der Engel</td>
<td valign="top" align="left">Engel</td>
<td valign="top" align="left">Bruno Cathomas</td>
<td valign="top" align="left">Theater Basel</td>
</tr>
<tr>
<td valign="top" align="left">2005</td>
<td valign="top" align="left">Fucking Åmål</td>
<td valign="top" align="left">Markus</td>
<td valign="top" align="left">Sebastian Nübling</td>
<td valign="top" align="left">Theater Basel,&nbsp;Junges Theater Basel</td>
</tr>
<tr>
<td valign="top" align="left">2003</td>
<td valign="top" align="left">Reiher</td>
<td valign="top" align="left">Aaron</td>
<td valign="top" align="left">Sebastian Nübling</td>
<td valign="top" align="left">Staatstheater Stuttgart,&nbsp;Junges Theater Basel</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p><span style="text-decoration: underline;"><strong>MUSIK FÜR THEATER, FILM UND HÖRSPIEL</strong></span></p>
<table style="width: 100%;" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td width="50" valign="top" align="left"><strong>Jahr</strong></td>
<td style="width: 450px;" valign="top" align="left"><strong>Titel</strong></td>
<td style="width: 200px;" valign="top" align="left"><strong>Regie</strong></td>
<td width="*" valign="top" align="left"><strong>Produktion</strong></td>
</tr>
<tr>
<td valign="top" align="left">&nbsp;</td>
<td valign="top" align="left">&nbsp;</td>
<td valign="top" align="left">&nbsp;</td>
<td valign="top" align="left">&nbsp;</td>
</tr>
<tr>
<td valign="top" align="left">2016</td>
<td valign="top" align="left">Troilus und Cressida</td>
<td valign="top" align="left">Rafael Sanchez</td>
<td valign="top" align="left">Schauspiel Köln</td>
</tr>
<tr>
<td valign="top" align="left">2015</td>
<td valign="top" align="left">Die Ermüdeten, oder das Etwas was wir sind! (UA)</td>
<td valign="top" align="left">Claudia Bauer</td>
<td valign="top" align="left">Schauspiel Leipzig</td>
</tr>
<tr>
<td valign="top" align="left">&nbsp;</td>
<td valign="top" align="left">Eigentlich schön!</td>
<td valign="top" align="left">Bruno Cathomas</td>
<td valign="top" align="left">Schauspiel Leipzig</td>
</tr>
<tr>
<td valign="top" align="left">2014</td>
<td valign="top" align="left">Die Irre von Chaillot</td>
<td valign="top" align="left">Anna Katharina Winkler</td>
<td valign="top" align="left">Landestheater Esslingen (Theater)</td>
</tr>
<tr>
<td valign="top" align="left">2013</td>
<td valign="top" align="left">Schneeschuhhasen im Glas</td>
<td valign="top" align="left">Anna Katharina Winkler</td>
<td valign="top" align="left">Landestheater Esslingen (Theater)</td>
</tr>
<tr>
<td valign="top" align="left">2012</td>
<td valign="top" align="left">Gegen die Mauer, die alles umgibt!</td>
<td valign="top" align="left">Jule Klink</td>
<td valign="top" align="left">Landestheater Esslingen (Theater)</td>
</tr>
<tr>
<td valign="top" align="left">&nbsp;</td>
<td valign="top" align="left">Startguthaben - Ein Leben im dritten A</td>
<td valign="top" align="left">Anna Katharina Winkler</td>
<td valign="top" align="left">Garage X Wien (Theater)</td>
</tr>
<tr>
<td valign="top" align="left">2011</td>
<td valign="top" align="left">Königs Moment</td>
<td valign="top" align="left">Anna Katharina Winkler</td>
<td valign="top" align="left">Landestheater Esslingen (Theater)</td>
</tr>
<tr>
<td valign="top" align="left">2009</td>
<td valign="top" align="left">Die Rächer</td>
<td valign="top" align="left">Katharina Herold</td>
<td valign="top" align="left">i-camp Theater München (Theater)</td>
</tr>
<tr>
<td valign="top" align="left">2009</td>
<td valign="top" align="left">Am besten tot</td>
<td valign="top" align="left">Bernd Blaschke</td>
<td valign="top" align="left">Münchner Kammerspiele (Theater)</td>
</tr>
<tr>
<td valign="top" align="left">2006</td>
<td valign="top" align="left">Mehrwert</td>
<td valign="top" align="left">Tim Staffel</td>
<td valign="top" align="left">WDR (Hörspiel)</td>
</tr>
</tbody>
</table>
<p><br><br> <span style="text-decoration: underline;"><strong>SONSTIGES</strong></span><br><br>Hörspiele beim BR und WDR. Leitung des Kinderspielclubs an der Landesbühne in Esslingen. Gründer von <em>Dirty Cat Records</em>. Über 15 Veröffentlichungen unter den Pseudonymen <em>Dirty Cat</em> und <em>Jan Plötzlich</em>. Mitgründer des Künstlerkollektivs <em>Hormonfabrik</em>.&nbsp;<br><br></p><p style="text-align: left;"><span style="text-decoration: underline;"><strong>KURZBIOGRAFIE</strong></span></p>
<p style="text-align: left;">Geboren in Arlesheim. Lernte Trompete, 
Gitarre, Klavier, Percussion. Von 2006 – 2010 machte er seine Ausbildung
 zum Schauspieler an der Otto-Falckenberg-Schule in München. Spielte vor
 der Schauspielausbildung am Jungen Theater Basel, Theater Basel und 
Staatstheater Stuttgart, u.a. in „Reiher“ (Regie: Sebastian Nübling, 
eingeladen zu den Wiener Festwochen), Fucking Åmål (Regie: Sebastian 
Nübling, Kurt Hübner Preis &amp; Pokal des Impulse Festivals) und in 
„Rückkehr der Engel“ (Regie: Bruno Cathomas). Während seiner Ausbildung 
war er an den Münchner Kammerspiele u.a. in „Mamma Medea“ (Regie: 
Stephan Kimmig) und in „Richard III“ (Regie: Claudia Bauer) zu sehen. 
2008 und 2009 wurde er mit dem Studienpreis des Migros-Kulturprozents 
und er Ernst Göhner Stiftung ausgezeichnet und 2010 für Junge Talente 
Schweiz ausgewählt. 2010-2014 war er fest an der Württembergischen 
Landesbühne Esslingen engagiert und wirkte in einigen Film Produktionen 
mit. Unter den Künstlernamen „Dirty Cat" und „Jan Plötzlich“ produziert 
Jonas Martin Schmid seit 2006 elektronische Musik. Seit 2007 hat er 
Releases bei diversen Labels und produzierte Remixes. Seit 2009 
produziert er auch Musik für Film und Theater, u. a. an den Münchner 
Kammerspielen, dem i-camp Theater München, Schauspiel Leipzig und der 
Württembergischen Landesbühne Esslingen. 2011&nbsp;gründete er das 
Musiklabel Dirty Cat Records und ist Mitgründer von dem 
Künstlerkollektiv "Hormonfabrik".&nbsp;Seit der Spielzeit 2014/15 ist 
Jonas Martin Schmid freiberuflich und wohnt in Berlin.</p>
<p>&nbsp;</p></div>

<div class="ccm-spacer"></div>

</div></div></div>
