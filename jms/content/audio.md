---
title: MUSIK
---

<div id="ccm-layout-wrapper-155" class="ccm-layout-wrapper">
<div id="ccm-layout-hauptbereich-16-2" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-2 ">
<div class="ccm-layout-row ccm-layout-row-1">

<div class="ccm-layout-16-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:58%">

<div id="blockStyle408HauptbereichLayout2Cell138" class=" ccm-block-styles">
<p><span>Jonas Martin Schmid aka Jan Plötzlich is producer, live-act, 
actor, musician and owner of Dirty Cat Records. He produces melodic 
downtempo/deep house. He combines deep beats and basses with light, 
beautiful melodies, guitar and trumpet sounds and vocals. His musical 
career began with trumpet, jazz, a guitar, a piano, a harmonica, a 
djembe and sentimental love-songs. This was followed by three years as a
 singer and guitarist in a new-wave / rockband. In search of new sounds,
 he began to combine his musical roots with electronic elements. It 
originated a mixture of techno, rock and house. Under the pseudonym 
„Dirty Cat“ he released his first EP on Filter Label (Macedonia)in 2008,
 more releases on Filter Label and Global Vortex Records followed.&nbsp;</span><br><span>During
 his education as an actor from 2006-2010 at the Otto Falckenberg School
 in Munich, Jonas Martin Schmid got the opportunity to write the music 
for a theater production for the first time. Here he was able to 
experiment, gain experience in different genres, expand his musical 
spectrum. From 2010 - 2014 he was employed as an actor at the 
Württembergische Landesbühne in Esslingen and was able to continue 
pursuing his career as a musician and composer for theater. In 2011 he 
founded his own label "Dirty Cat Records", a label for electronic music,
 with the aim to promote young artists. Up to now, more than 20 artists 
have published over 40 releases. In the end of 2014 he moved to Berlin 
to focus on his musical career and to write a movie script and the 
associated music. As a theatre musician he received assignements at 
Schauspiel Leipzig and Schauspiel Köln. For his movie script, he began 
using the pseudonym "Jan Plötzlich" to produce downtempo music and apply
 to another label. He had his first live-act as Jan Plötzlich at "Rave 
im Grünen" in May 2015. Shortly thereafter, he organized his first party
 with his newly founded artists' collective "Hormonfabrik", under the 
name "The country of the five senses" at Loftus Hall in Berlin. It was 
followed by appearances in Suicide Circus Berlin, at the 3000 Grad 
Festival and at the Sinneswerwachen Open Air. In June 2015, Jonas Martin
 Schmid released his first EP under his pseudonym „Jan Plötzlich“ at 
3000 Grad Records, called „Cascade“, followed by an album on "Kraak 
Records" from Greece, remixes on his own label and more successful 
parties with „Hormonfabrik“.</span></p>
</div>

<div id="blockStyle431HauptbereichLayout2Cell141" class=" ccm-block-styles"><div id="HTMLBlock431" class="HTMLBlock">
  <iframe width="100%" height="200" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/144190729&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
</div></div>

</div>

<div class="ccm-layout-16-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:41.99%"><p style="text-align: left;"><img src="img/jonasmartinschmid-p3.jpg" alt="jonasmartinschmid-p3.jpg" width="400" height="533"></p></div>

<div class="ccm-spacer"></div>
</div></div></div>

<div id="ccm-layout-wrapper-154" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-17-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-17-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:100%">

<p>&nbsp;</p><p><span style="font-size: medium;">Theater Soundtrack zu "Die Ermüdeten oder Das Etwas, das wir sind (UA)" <br /></span>Regie: Claudia Bauer, Bühne/Kostüme: Andreas Auerbach, Dramaturgie: Matthias Huber, Video: Gabriel Arnold, Licht: Veit-Rüdiger Griess, Musik: Jonas Martin Schmid, Premiere: 25.09.2015, Schauspiel Leipzig</p>

<div id="jquery_jplayer_325" class="jp-jplayer"></div>
<div class="blue_monday">
<div id="jp_container_325" class="jp-audio">
<div class="jp-type-playlist">
<div class="jp-gui jp-interface">
<ul class="jp-controls">
<li><a href="javascript:;" class="jp-previous" tabindex="1">previous</a></li>
<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
<li><a href="javascript:;" class="jp-next" tabindex="1">next</a></li>
<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
</ul>
<div class="jp-current-title"></div>
<div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div>
<div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div>
<div class="jp-time-holder"><div class="jp-current-time"></div><div class="jp-duration"></div></div>
<ul class="jp-toggles">
<li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
<li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
</ul>
<div class="jp-album-art"></div>
</div>
<div class="jp-playlist"><ul><li></li></ul></div>
<div class="jp-no-solution"><span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.</div>
</div>
</div>
</div>

<p>&nbsp;</p><p><span style="font-size: medium;">Theater Soundtrack zu "Eigentlich schön!"&nbsp;<br></span>Regie: Bruno Cathomas, Bühne: Hugo Gretler, Kostüme: Agathe McQueen, Dramaturgie: Alexander Elsner, Musik: Jonas Martin Schmid,&nbsp;Premiere: 20.03.2015, Schauspiel Leipzig</p>

<div id="jquery_jplayer_296" class="jp-jplayer"></div>
<div class="blue_monday">
<div id="jp_container_296" class="jp-audio">
<div class="jp-type-playlist">
<div class="jp-gui jp-interface">
<ul class="jp-controls">
<li><a href="javascript:;" class="jp-previous" tabindex="1">previous</a></li>
<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
<li><a href="javascript:;" class="jp-next" tabindex="1">next</a></li>
<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
</ul>
<div class="jp-current-title"></div>
<div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div>
<div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div>
<div class="jp-time-holder"><div class="jp-current-time"></div><div class="jp-duration"></div></div>
<ul class="jp-toggles">
<li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
<li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
</ul>
<div class="jp-album-art"></div>
</div>
<div class="jp-playlist"><ul><li></li></ul></div>
<div class="jp-no-solution"><span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.</div>
</div>
</div>
</div>

<p>&nbsp;</p><p><span style="font-size: medium;">Theater Soundtrack zu "Die Irre von Chaillot"&nbsp;<br></span>Regie: Anna Katharina Winkler, Ausstattung: Markus Wagner, Musik: Jonas Martin Schmid,&nbsp;Premiere: 10.04.2014, Württembergische Landesbühne Esslingen</p>

<div id="jquery_jplayer_305" class="jp-jplayer"></div>
<div class="blue_monday">
<div id="jp_container_305" class="jp-audio">
<div class="jp-type-playlist">
<div class="jp-gui jp-interface">
<ul class="jp-controls">
<li><a href="javascript:;" class="jp-previous" tabindex="1">previous</a></li>
<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
<li><a href="javascript:;" class="jp-next" tabindex="1">next</a></li>
<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
</ul>
<div class="jp-current-title"></div>
<div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div>
<div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div>
<div class="jp-time-holder"><div class="jp-current-time"></div><div class="jp-duration"></div></div>
<ul class="jp-toggles">
<li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
<li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
</ul>
<div class="jp-album-art"></div>
</div>
<div class="jp-playlist"><ul><li></li></ul></div>
<div class="jp-no-solution"><span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.</div>
</div>
</div>
</div>

<p>&nbsp;</p><p><span style="font-size: medium;">Theater Soundtrack zu "Königs Moment"&nbsp;<br></span>Regie: Anna Katharina Winkler, Bühne: Thomas Kurz, Musik: Jonas Martin Schmid,&nbsp;Premiere: 26.01.2012, Landesbühne Esslingen</p>

<div id="jquery_jplayer_204" class="jp-jplayer"></div>
<div class="blue_monday">
<div id="jp_container_204" class="jp-audio">
<div class="jp-type-playlist">
<div class="jp-gui jp-interface">
<ul class="jp-controls">
<li><a href="javascript:;" class="jp-previous" tabindex="1">previous</a></li>
<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
<li><a href="javascript:;" class="jp-next" tabindex="1">next</a></li>
<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
</ul>
<div class="jp-current-title"></div>
<div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div>
<div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div>
<div class="jp-time-holder"><div class="jp-current-time"></div><div class="jp-duration"></div></div>
<ul class="jp-toggles">
<li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
<li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
</ul>
<div class="jp-album-art"></div>
</div>
<div class="jp-playlist"><ul><li></li></ul></div>
<div class="jp-no-solution"><span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.</div>
</div>
</div>
</div>

<p>&nbsp;</p><p><span style="font-size: medium;">Theater Soundtrack zu "Die Rächer"&nbsp;<br></span>Regie: Katharina Herold, Ausstattung: Teresa Vergho, Musik: Jonas Martin Schmid,&nbsp;Premiere: 28.07.2009, i-camp München</p>

<div id="jquery_jplayer_210" class="jp-jplayer"></div>
<div class="blue_monday">
<div id="jp_container_210" class="jp-audio">
<div class="jp-type-playlist">
<div class="jp-gui jp-interface">
<ul class="jp-controls">
<li><a href="javascript:;" class="jp-previous" tabindex="1">previous</a></li>
<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
<li><a href="javascript:;" class="jp-next" tabindex="1">next</a></li>
<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
</ul>
<div class="jp-current-title"></div>
<div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div>
<div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div>
<div class="jp-time-holder"><div class="jp-current-time"></div><div class="jp-duration"></div></div>
<ul class="jp-toggles">
<li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
<li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
</ul>
<div class="jp-album-art"></div>
</div>
<div class="jp-playlist"><ul><li></li></ul></div>
<div class="jp-no-solution"><span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.</div>
</div>
</div>
</div>

</div><div class="ccm-spacer"></div></div></div></div>
