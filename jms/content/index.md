---
title: AKTUELLES
---

<div id="ccm-layout-wrapper-209" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-11-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-11-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:55%"><h2><span style="text-decoration: underline;">Musik</span></h2>
<h3>Jan Plötzlich</h3>
<p>12.12.2015 live @ Das Land der fünf Sinne - Loftus Hall, Maybachufer 48, Berlin</p>
<h3>Troilus und Cressida von W. Shakespeare</h3>
<p>Premiere: Schauspiel Köln (29.01.2016)<br>Regie: Rafael Sanchez<br>Bühne: Simeon Meier<br>Kostüme: Birgit Bungum<br>Musik: Jonas Schmid<br>Dramaturgie: Nina Rühmeier<br><a href="http://www.schauspielkoeln.de/spielplan/premieren/troilus-und-cressida/" target="_blank">&gt;&nbsp;Weitere Informationen und Termine</a></p>
<h3>Die Ermüdeten von Bernhard Studlar</h3>
<p>Premiere: Diskothek, Schauspiel Leipzig (25.09.2015)<br>Regie: Claudia Bauer<br>Bühne &amp; Kostüme: Andreas Auerbach<br>Musik: Jonas Schmid<br>Licht: Veit-Rüdiger Griess<br>Video: Gabriel Arnold<br>Dramaturgie: Matthias Huber&nbsp;<br><a href="http://www.schauspiel-leipzig.de/buehnen/diskothek/inszenierungen/eigentlich-schoen-ua/" target="_blank">&gt;&nbsp;Weitere Informationen und Termine</a></p>
<h3>Eigentlich schön! von Volker Schmidt</h3>
<p>Premiere: Diskothek, Schauspiel Leipzig (20.03.2015)<br>Regie: Bruno Cathomas<br>Bühne: Hugo Gretler<br>Kostüme: Agathe MacQueen<br>Musik: Jonas Martin Schmid<br>Dramaturgie: Alexander Elsner<br><a href="http://www.schauspiel-leipzig.de/buehnen/diskothek/inszenierungen/eigentlich-schoen-ua/" target="_blank">&gt;&nbsp;Weitere Informationen und Termine</a></p>
<h3>&nbsp;</h3></div><div class="ccm-layout-11-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:44.99%"><p style="text-align: left;">&nbsp;<img src="img/jonasmartinschmid-p1.jpg" alt="jonasmartinschmid-p1.jpg" width="400" height="306"></p><h2><span style="text-decoration: underline;">Film</span></h2>
<h3>Teaser "Inside Me" online!</h3>
<p>Der Teaser für den Kinofilm "Inside Me" ist nun online!<br><a href="https://www.youtube.com/watch?v=HVUjDOcgNUs" target="_blank">Deutsch</a>&nbsp;|&nbsp;<a href="https://www.youtube.com/watch?v=HVUjDOcgNUs" target="_blank">Englisch</a></p>
<p>&nbsp;</p>
<h2><span style="text-decoration: underline;">Theater</span></h2>
<h3>Tödliche Schönheit und Grimmige WalkActs!</h3>
<p>Ich spiele ab November beim Papilliotheater in "Tödliche Schönheit", als <em>Olaf</em> und &nbsp;Kommissar <em>Malovski</em> und in "Grimmige WalkActs", Märchen für Erwachsene. Weitere Infos auf der Seite des Papilliotheaters.<br><a href="http://www.papiliotheater.de/" target="_blank">http://www.papiliotheater.de/</a></p></div><div class="ccm-spacer"></div></div></div></div>
