---
title: Impressum
---

<div id="ccm-layout-wrapper-159" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-23-2" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-2 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-23-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:50%"><div id="blockStyle451HauptbereichLayout2Cell143" class=" ccm-block-styles">
<p><span style="font-size: small;">Jonas Martin Schmid</span><br>Net: www.jonasmartinschmid.ch<br>Mail: jonasschmid[at]gmx.ch<br><br></p>
<p><span class="textbold" style="font-size: small;">Haftungshinweis</span><br><span>Die
 Mitteilung sämtlicher Informationen auf diesem Website erfolgt nach 
bestem Wissen und Gewissen, jedoch “ohne Gewähr” für die Richtigkeit und
 Vollständigkeit. Jegliche Haftung für Schäden, die durch Fehler auf 
diesem Website entstehen, ist, soweit gesetzlich zulässig, 
ausgeschlossen. Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir
 keine Haftung für die Inhalte externer Links. Für den Inhalt der 
verlinkten Seiten sind ausschließlich deren Betreiber 
verantwortlich.&nbsp;</span></p></div></div><div class="ccm-layout-23-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:49.99%"><p><img src="img/jonasmartinschmid-p7.jpg" alt="jonasmartinschmid-p7.jpg" width="450" height="600"></p></div><div class="ccm-spacer"></div></div></div></div><div id="ccm-layout-wrapper-158" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-24-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-24-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:100%">&nbsp;</div><div class="ccm-spacer"></div></div></div></div>
