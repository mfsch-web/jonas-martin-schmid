---
title: SPRECHEN
---

<div id="ccm-layout-wrapper-151" class="ccm-layout-wrapper"><div id="ccm-layout-hauptbereich-18-1" class="ccm-layout ccm-layout-table  ccm-layout-name-Hauptbereich-Layout-1 "><div class="ccm-layout-row ccm-layout-row-1"><div class="ccm-layout-18-col-1 ccm-layout-cell ccm-layout-col ccm-layout-col-1 first" style="width:50%"><div id="blockStyle413HauptbereichLayout1Cell140" class=" ccm-block-styles">

<div id="jquery_jplayer_201" class="jp-jplayer"></div>

<div class="blue_monday interface-hide-artwork interface-hide-repeat">
<div id="jp_container_201" class="jp-audio">
<div class="jp-type-single">
<div class="jp-gui jp-interface">
<ul class="jp-controls">
<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
</ul>
<div class="jp-current-title"></div>
<div class="jp-progress"><div class="jp-seek-bar"><div class="jp-play-bar"></div></div></div>
<div class="jp-volume-bar"><div class="jp-volume-bar-value"></div></div>
<div class="jp-time-holder"><div class="jp-current-time"></div><div class="jp-duration"></div></div>
<ul class="jp-toggles">
<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
</ul>
<div class="jp-album-art"></div>
</div>
<div class="jp-title"><ul><li><span class="jp-current-title"></span> <span class="jp-single-download"></span></li></ul></div>
<div class="jp-no-solution"><span>Update Required</span> To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.</div>
</div>
</div>
</div>

</div></div><div class="ccm-layout-18-col-2 ccm-layout-cell ccm-layout-col ccm-layout-col-2 last" style="width:49.99%"><p style="text-align: left;"><img src="img/jonasmartinschmid-p4.jpg" alt="jonasmartinschmid-p4.jpg" width="400" height="600"></p></div><div class="ccm-spacer"></div></div></div></div>
